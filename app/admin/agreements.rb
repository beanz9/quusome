# encoding: UTF-8

ActiveAdmin.register Agreement do

  menu :label => "이용약관"

  index do
    selectable_column
    column :title
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :description do
        simple_format agreement.description
      end
      row :created_at
      row :updated_at
    end

  end

  form do |f|
    f.inputs "Agreement Details" do
      f.input :title
      f.input :description
    end
    f.actions
  end



end
