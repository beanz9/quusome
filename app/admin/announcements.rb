# encoding: UTF-8
ActiveAdmin.register Announcement do

  menu :label => "알림"

  #scope :all
  #scope :public, :default => true
  #scope :private
  #scope :info_type
  #scope :warning_type
  #scope :not_expire
  #scope :expire

  filter :title
  filter :body, :label => "내용"
  filter :user_user_id, :as => :string, :label => "USER"
  filter :expire_date, :label => "만료날짜"
  filter :created_at, :label => "생성날짜"


  index do
    selectable_column
    column :title
    column :expire_date
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :body
      row :expire_date
      row :user
      row :announcement_type
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs "Announcement Details" do
      f.input :title
      f.input :body
      f.input :expire_date
      f.input :announcement_type
    end
    f.actions
  end

end
