# encoding: UTF-8

ActiveAdmin.register PrivacyPolicy do

  menu :label => "개인정보취급방침"

  index do
    selectable_column
    column :title
    column :created_at
    column :updated_at
    default_actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :description do
        simple_format privacy_policy.description
      end
      row :created_at
      row :updated_at
    end
  end

end
