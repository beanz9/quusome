# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->


hide_announcement = (announcement_created_at) ->
  create_cookie(announcement_created_at, 'hidden', 365)


create_cookie = (name, value, days) ->
  if days
    date = new Date()
    date.setTime(date.getTime() + (days*24*60*60*1000))
    expires = "; expires=" + date.toGMTString();
  else
    expires = ""
    document.cookie = name + "=" + value + expires + "; path=/"

