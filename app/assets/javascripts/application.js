// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-migrate-1.2.1
//= require jquery_ujs
//= require jquery.remotipart
//= require jquery-ui
//= require bootstrap-all
//= require bootstrap-modal-ext
//= require bootstrap-modalmanager
//= require jquery_nested_form
//= require jquery.validate
//= require spin
//= require jquery.spin
//= require overthrow.min
//= require jquery.nanoscroller
//= require bootstrap-select
//= require jquery_nested_form
//= require jquery.lazyload.min
//= require bootstrapx-clickover
//= require tag-it.min
//= require private_pub
//= require jquery.mu.image.resize
//= require imgLiquid
//= require jquery.mousewheel
//= require ilightbox.packed
//= require jquery.mixitup.min
//= require jquery.app-folders
//= require jquery.backstretch
//= require vague
//= require jquery.foggy
//= require_tree .

$(function() {


    var nano = $(".nano");
    nano.nanoScroller();


    $('.container').on('click','#message_icon', function() {
        var count = $('#message_count_badge').text() == '' ? '0' :$('#message_count_badge').text();
        if(parseInt(count) != 0) {

            $.ajax({
                url: "/message_read_handler",
                type: "post",
                error: function(jqXHR, textStatus, errorthrown) {
                    return "문제가 발생했습니다."
                },
                success: function(data, textStatus, jqXHR) {

                }

            });
        }
    });

    $('.container').on('click','#agent_request_icon', function() {

        var count = $('#agent_request_count_badge').text() == '' ? '0' :$('#agent_request_count_badge').text();
        if(parseInt(count) != 0) {
            $.ajax({
                url: "/agent_request_read_handler",
                type: "post",
                error: function(jqXHR, textStatus, errorthrown) {
                    return "문제가 발생했습니다."
                },
                success: function(data, textStatus, jqXHR) {

                }

            });
        }
    });

    $('.container').on('click','#showcase_update_icon', function() {

        var count = $('#showcase_update_count_badge').text() == '' ? '0' :$('#showcase_update_count_badge').text();
        if(parseInt(count) != 0) {
            $.ajax({
                url: "/showcase_update_read_handler",
                type: "post",
                error: function(jqXHR, textStatus, errorthrown) {
                    return "문제가 발생했습니다."
                },
                success: function(data, textStatus, jqXHR) {

                }

            });
        }
    });

    $('#showcase_update_scroll').on('scroll', function(e) {
        if ($('#showcase_update_scroll').outerHeight() > ($('#showcase_update_scroll').get(0).scrollHeight - $('#showcase_update_scroll').scrollTop() - 10  )) {
            if ($('#showcase_update_scroll_is_loading').val() == 'false' && $('#showcase_update_scroll_end').val() == 'false') {
                //$('#message_scroll_page').val(parseInt($('#message_scroll_page').val()) + 1);
                $('#showcase_update_scroll_is_loading').val('true');
                $.ajax({
                    url: "/retrieve_showcase_update",
                    type: "get",
                    data: "showcase_update_scroll_page=" + $('#showcase_update_scroll_page').val(),
                    error: function(jqXHR, textStatus, errorthrown) {
                        return "문제가 발생했습니다."
                    },
                    success: function(data, textStatus, jqXHR) {

                    }

                });
            }

        }
    });


    $('#message_scroll').on('scroll', function(e) {
        if ($('#message_scroll').outerHeight() > ($('#message_scroll').get(0).scrollHeight - $('#message_scroll').scrollTop() - 10  )) {
            if ($('#message_scroll_is_loading').val() == 'false' && $('#message_scroll_end').val() == 'false') {
                //$('#message_scroll_page').val(parseInt($('#message_scroll_page').val()) + 1);
                $('#message_scroll_is_loading').val('true');
                $.ajax({
                    url: "/retrieve_message",
                    type: "get",
                    data: "message_scroll_page=" + $('#message_scroll_page').val(),
                    error: function(jqXHR, textStatus, errorthrown) {
                        return "문제가 발생했습니다."
                    },
                    success: function(data, textStatus, jqXHR) {

                    }

                });
            }

        }
    });

    $('#agent_request_scroll').on('scroll', function(e) {
        if ($('#agent_request_scroll').outerHeight() > ($('#agent_request_scroll').get(0).scrollHeight - $('#agent_request_scroll').scrollTop() - 10  )) {
            if ($('#agent_request_scroll_is_loading').val() == 'false' && $('#agent_request_scroll_end').val() == 'false') {
                //$('#message_scroll_page').val(parseInt($('#message_scroll_page').val()) + 1);
                $('#agent_request_scroll_is_loading').val('true');
                $.ajax({
                    url: "/retrieve_agent_request",
                    type: "get",
                    data: "agent_request_scroll_page=" + $('#agent_request_scroll_page').val(),
                    error: function(jqXHR, textStatus, errorthrown) {
                        return "문제가 발생했습니다."
                    },
                    success: function(data, textStatus, jqXHR) {

                    }

                });
            }

        }
    });



});

function hide_announcement(announcement_id) {
    create_cookie(announcement_id, 'hidden', 365);
    $("#" + announcement_id).click();

}


function create_cookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}







