# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('.container-fluid').on 'click','#message_submit_btn', ->

    if $('#message_body').val() == ''
      alert("메세지를 입력하세요.")
      $('#message_body').focus()

      return

    $('#message_submit').click()
    $('#message_body').val('')
