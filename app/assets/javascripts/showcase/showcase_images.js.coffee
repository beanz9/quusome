# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->

  #저장버튼 클릭
  $('#ImageModal').on 'click', '#showcase_image_upload_btn', ->
    $('#showcase_image_upload_btn').addClass('disabled')
    $('#showcase_image_close_btn').addClass('disabled')

    $('#image_spin').show().spin(
      lines: 10
      length: 5
      width: 3
      radius: 5
      color: 'white'
      speed: 1
      trail: 60
      shadow: false
    )

    button_disabled()

    $('#showcase_image_upload_submit').click()


  button_disabled = ->
    $('#showcase_image_upload_btn').addClass('disabled')
    $('#showcase_image_close_btn').addClass('disabled')
    $('#showcase_image_upload_btn').attr('disabled', 'disabled')
    $('#showcase_image_close_btn').attr('disabled', 'disabled')
    $('.close').attr('disabled', 'disabled')






  #체크박스 클릭 이벤트
  $('.image_upload_div').on 'click','.chk', ->
    $($(@).siblings('.chk')).not($(@)).removeAttr('checked')
    $(@).attr('checked', $(@).attr('checked'))

    if $(@).val() == 'upload'
      $(@).parent().parent().siblings('.image_url').css('display', 'none')
      $(@).parent().parent().siblings('.image_upload').css('display', '')
    else
      $(@).parent().parent().siblings('.image_url').css('display', '')
      $(@).parent().parent().siblings('.image_upload').css('display', 'none')


  $('.image_upload_div').on 'change', '.image_upload_radio', ->
    if $(@).val() == 'upload'
      $(@).parent().parent().siblings('.image_url').css('display', 'none')
      $(@).parent().parent().siblings('.image_upload').css('display', '')
      $(@).parent().parent().siblings('.image_url').children().val('')
    else
      $(@).parent().parent().siblings('.image_url').css('display', '')
      $(@).parent().parent().siblings('.image_upload').css('display', 'none')
      $(@).parent().parent().siblings('.image_upload ').children().val('')

  #추가버튼 클릭 이벤트
  $('.modal-body').on 'click', '#image_upload_add_btn',  ->
    $('.image_upload_div').append(make_html(false, $('.image_upload_div').children().length))




  #추가된 행 삭제 이벤트
  $('.image_upload_div').on 'click','.delete_image_upload_row', ->
    $(@).parent().parent().parent().remove()


  #모달 창 초기화
  $('#ImageModal').on 'hide', ->
    $('.image_upload_div').html(make_html(true, 0))




  make_html = (init, count) ->
    html = '<div class="image_upload_row">'
    html += '<div class="image_type">'
    html += '<label><input type="radio" name="showcase_images[' + count + '][type]" class="image_upload_radio" checked="checked" value="upload"> <span>업로드</span></label>'
    html += '<label><input type="radio" name="showcase_images[' + count + '][type]" class="image_upload_radio"  value="url"> <span>URL</span></label>'
    unless init
      html += '<span style="margin-left: 370px; clear: both;"><a class="delete_image_upload_row" href="#">X</a></span>'
    html += '</div>'
    html += '<div class="image_upload" style="width:510px; height:20px; background-color: #e9eaea; padding:10px;">'
    html += '<input type="file" name="showcase_images[' + count + '][image]" style="line-height: 10px; font-size: 12px;">'


    html += '</div>'
    html += '<div class="image_url" style="width:510px; height:20px; background-color: #e9eaea; padding:10px; display: none;">'
    html += 'URL : <input type="text" name="showcase_images[' + count + '][url]">'


    html += '</div>'
    html += '<div style="width:530px; background-color: #e9eaea; " class="image_desc">'
    html += '<span style="margin-left: 10px; margin-top: 10px;">설명 : <textarea name="showcase_images[' + count + '][caption]" rows="2" style="width:440px"></textarea> </span>'
    html += '</div>'

    html += '<div style="width:530px; background-color: #e9eaea; " class="image_desc">'
    html += '<span style="margin-left: 10px;">링크 URL : <input type="text" name="showcase_images[' + count + '][link]" style="width:410px"/> </span>'
    html += '</div>'

    html += '</div>'





























