# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->
  #$(".image_container img").imgCenter(
  #  parentSteps: 1
  #  complete: ->
  #    $('.image_container img').fadeIn(300)
  #)

  $('.showcase_index_image').muImageResize(
    width: 275
    height: 160
  )

#  $('.showcase_show_image').muImageResize(
#    width: 192
#    height: 140
#  )

  ilight_modal = $('.ilightbox').iLightBox(
    path: 'horizontal'

    overlay: {
      opacity: 0.9
    }
    maxScale: 1
    callback: {
      onRender: (api) ->
#        $('.user_info_reward_icon').tooltip(
#          html: true
#
#        )



    }

  )




  $('.showcase_image_container').imgLiquid(
    fill: true
    #fadeInTime: 500
  )




#  filteredData = ''
#  holder = $('#content_filter')
#  data = holder.clone()


#  $('#source_filter button').on click: ->
#
#
#    if !$(@).hasClass('active')
#      if $(@).attr('data-id') == 'all_filter'
#        filteredData = data.find('li');
#      else
#        filteredData = data.find('li[data-type=' + $(@).attr('data-id') + ']');
#    else
#      return false
#
#    option =
#      duration:500
#      easing:"easeInOutQuad"
#      adjustHeight:"auto"
#      useScaling:!0
#
#
#
#    holder.quicksand(filteredData, option, ->
#
#
#      #$('#content_filter').html(filteredData)
#      alert(filteredData.length)
#      #alert($('.ilightbox').length)
#      $('.ilightbox').iLightBox(
#        path: 'horizontal'
#        overlay: {
#        opacity: 0.9
#        }
#      )
#
#    )


  $('#content_grid').mixitup(


  )



  #$(".showcase_image_container img").imgCenter(
  #  parentSteps: 1
  #)


  #$(".imgLiquid").imgLiquid(
  #  fill: true
  #);

  #$('#myImage').resizeToParent();

  #$('.image_thumbs').realshadow(
  #
  #  inset: true
  #  length: 5
  #)

  #에이전트 popver
  $('[rel="clickover"]').clickover(
    html: true
    content: ->
      $($(@).attr('data-id')).html()
  )

  #에이전트 미션 셀렉트 박스
  $('.selectpicker').selectpicker(
    style: 'btn-info'
  )

  #에이전트 미션 셀렉트 박스 이벤트
  #$('#showcase_mission_term').on change: ->
  #  if $('#showcase_mission_term').val() != ''
  #    $('#showcase_mission_info').removeAttr('disabled')
  #  else
  #    $('#showcase_mission_info').attr('disabled', 'disabled')


  #저장
  $('#showcase_save').on click: ->
    if validate()
      $('#showcase_is_publish').val("0")
      $('#showcase_submit').click()

  #저장 후 퍼블리쉬
  $('#showcase_publish').on click: ->
    if validate()
      $('#showcase_is_publish').val("1")
      $('#showcase_submit').click()


  #수정
  $('.showcase_update').on click: ->
    if validate()
      if $(@).attr("id") == "private_update"
        $('#is_publish').val("0")
      else if $(@).attr("id") == "public_update"
        $('#is_publish').val("1")


      $('#showcase_submit').click()


  validate = ->
    if $('#showcase_title').val() == ''
      alert("쇼케이스 타이틀을 입력하세요.")
      $('#showcase_title').focus()
      return false
    else if $('#showcase_title').val().length > 30
      alert("30자 이내로 입력하세요.")
      $('#showcase_title').focus()
      return false
    else if $('#showcase_info').val() == ''
      alert("쇼케이스 설명을 입력하세요.")
      $('#showcase_info').focus()
      return false
    else if $('#showcase_info').val().length > 250
      alert("250자 이내로 입력하세요.")
      $('#showcase_info').focus()
      return false
#    else if $('#showcase_mission_term').val() == ''
#      alert("쇼케이스 미션기간을 선택하세요.")
#      $('#showcase_mission_term').css("display", "")
#      $('#showcase_mission_term').focus()
#      $('#showcase_mission_term').css("display", "none")
#      return false
#    else if $('#showcase_mission_info').val() == ''
#      alert("쇼케이스 미션을 입력하세요.")
#      $('#showcase_mission_info').focus()
#      return false
    else if $('#image_upload_thumb').children().length == 0 && $('#video_thumb').children().length == 0
      alert("이미지나 동영상을 등록하세요.")
      return false

    return true

  $('.ContentRegis').on 'click', '.upload_image .icon_remove', ->
    if confirm("선택한 사진을 삭제하시겠습니까?")
      object = $(@).parent().parent()

      type = object.children('.source_type').val()
      image_id = object.children('.showcase_image_id').val()
      content_id = object.children('.content_id').val()
      object.remove()

      #수정페이지
#      if content_id
#        $.ajax "/contents/#{content_id}?type=#{type}",
#
#          type: "DELETE"
#          error: (jqXHR, textStatus, errorThrown) ->
#            alert("삭제 중 문제가 발생했습니다.")
#          success: (data, textStatus, jqXHR) ->
#            object.remove()


      #등록페이지
#      else
#        if type == 'upload'
#          $.ajax "/showcase_image/#{image_id}",
#
#            type: 'DELETE'
#            error: (jqXHR, textStatus, errorThrown) ->
#              alert("삭제 중 문제가 발생했습니다.")
#            success: (data, textStatus, jqXHR) ->
#              object.remove()
#        else
#          object.remove()

      $('.nano').nanoScroller()

  #유튜브 삭제 이벤트
  $('.ContentRegis').on 'click', '.video_upload .icon_remove', ->
    if confirm("선택한 동영상을 삭제하시겠습니까?")

      object = $(@).parent().parent()
      type = object.children('.source_type').val()
      content_id = object.children('.content_id').val()
      object.remove()
#      if content_id
#        $.ajax "/contents/#{content_id}?type=#{type}",
#
#          type: "DELETE"
#          error: (jqXHR, textStatus, errorThrown) ->
#            alert("삭제 중 문제가 발생했습니다.")
#          success: (data, textStatus, jqXHR) ->
#            object.remove()
#      else
#        object.remove()

      $('.nano').nanoScroller()

  $('.ContentRegis').on 'click', '.upload_video', ->
      $(@).empty()

  $('.AgentRegis').on 'click', '.seleted_reward', ->
    if confirm("선택한 선물을 삭제하시겠습니까?")
      showcase_reward_id = $(@).children('.showcase_reward_id').val()
      object = $(@)
      if showcase_reward_id
        $.ajax "/showcase_rewards/#{showcase_reward_id}",
          type: "DELETE"
          error: (jqXHR, textStatus, errorThrown) ->
            alert("삭제 중 문제가 발생했습니다.")
          success: (data, textStatus, jqXHR) ->
            object.remove()
            alert("삭제되었습니다.")
      else
        object.remove()


  #저장버튼 클릭 이벤트
  $('.btn-group').on 'click', '.agent_save_btn', (e) ->
    reward_item_id = $(@).attr('reward_item_id')
    reward_detail_info = $(@).parent().prev().val()
    selected_reward_count = parseInt($('#selected_reward_count').val())


    if reward_duplicate_check($('.reward_item_id'), reward_item_id)
      html = ''
      html += '<li style="margin-left: 5px; margin-bottom: 5px;" class="seleted_reward">'
      html += '<input type="hidden" class="reward_item_id" name="showcase[showcase_rewards_attributes][' + selected_reward_count + '][reward_item_id]" value="' + reward_item_id + '">'
      html += '<input type="hidden" name="showcase[showcase_rewards_attributes][' + selected_reward_count + '][detail_info]" value="' + reward_detail_info + '">'
      html += '<a style="cursor: pointer" class="thumbnail"><img src="/assets/45x45/' + $(@).attr('reward_item_image') + '"></a>'
      html += '</li>'

      $(html).appendTo('.selected_rewards').hide().fadeIn(1000)

      selected_reward_count = selected_reward_count + 1
      $('#selected_reward_count').val(reward_item_id)

    $('.btn-group').trigger('click')

  reward_duplicate_check = (obj, id) ->
    result = true
    obj.each ->
      if id == $(@).val()
        alert("이미 선택했습니다.")
        result =  false
      else
        result =  true

    result

  #에이전트 신청보내기
  $('#agent_request_btn').on 'click', ->
    if $('#current_user').val() == ''
      alert("로그인이 필요합니다.")
      location.href = "/users/sign_in"
      return false


    if confirm('에이전트 신청을 보내시겠습니까?')
      $('#agent_request_submit').click()


  $('a#favorite_link').bind "ajax:error", (e, data, status, xhr) ->

    if data.status == 406
      alert("로그인이 필요합니다.")
      location.href = "/users/sign_in"

  $('form#message_form').bind "ajax:error", (e, data, status, xhr) ->

    if data.status == 406
      alert("로그인이 필요합니다.")
      location.href = "/users/sign_in"

  $('.showcase_info_btn').on 'click', (e) ->
    e.preventDefault()
    $('.showcase_info').slideToggle('fast')

  $('#image_upload_thumb').sortable(
    forcePlaceholderSize: true
    handle: '.icon_move'
    placeholder: "sortable-placeholder"
    revert: 150
    scroll: true
    scrollSensitivity: 10
    scrollSpeed: 40
    update: ->
      $(@).children().each ->
        index = $(@).index()

        if $(@).attr('class') == 'upload_image thumbnail_content'

          $(@).find('.source_type').attr("name", "showcase[contents_attributes][#{index}][source_type]")
          $(@).find('.caption').attr("name", "showcase[contents_attributes][#{index}][caption]")
          $(@).find('.showcase_image_id').attr("name", "showcase[contents_attributes][#{index}][showcase_image_id]")
          $(@).find('.url').attr("name", "showcase[contents_attributes][#{index}][url]")
          $(@).find('.link').attr("name", "showcase[contents_attributes][#{index}][link]")
          $(@).find('.thumbnail_index').attr("id", "thumbnail_index_#{index}")
        else
          $(@).find('.source_type').attr("name", "showcase[contents_attributes][#{index}][source_type]")
          $(@).find('.caption').attr("name", "showcase[contents_attributes][#{index}][caption]")
#          $(@).find('.showcase_image_id').attr("name", "showcase[contents_attributes][#{index}][showcase_image_id]")
          $(@).find('.source_id').attr("name", "showcase[contents_attributes][#{index}][source_id]")
          $(@).find('.url').attr("name", "showcase[contents_attributes][#{index}][url]")


  )

  $('.ContentRegis').on 'hover', '.thumbnail_content', (e) ->
    e.preventDefault()
    $(@).find('.thumbnails_icon').fadeToggle("fast")

  $('.ContentRegis').on 'click', '.icon_modify', (e) ->
    $('.image_url').attr("src", $(@).parent().next().attr("src")).addClass("register_thumb")

    $('.content_modal_caption').val($(@).parent().parent().find('.caption').val())
    $('.content_modal_link').val($(@).parent().parent().find('.link').val())
    $('.thumbnail_modal_index').val($(@).parent().parent().find('.thumbnail_index').attr("id"))
    $('#content_modal').modal()

  $('.content_modal_apply_btn').on 'click', ->
    obj = $("#" + $('.thumbnail_modal_index').val()).parent()

    obj.find('.caption').val($('.content_modal_caption').val())
    obj.find('.link').val($('.content_modal_link').val())
    $('.content_modal_close_btn').click()

  #$('.guide_image').slideDown()



