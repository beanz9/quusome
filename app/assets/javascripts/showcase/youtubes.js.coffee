# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->

  $(".lazy").lazyload(
    threshold : 500

    container: $("#youtube_search_result_div")
  )




  #검색 엔터 이벤트
  $('#youtube_query').on 'keyup', (e) ->
    if e.keyCode == 13
      $('#youtube_body').children().remove()
      $('#youtube_page').val('1')
      $('#youtube_submit').click()
      activity_indicator_start()


  #소스입력 중 미리보기 이벤트
  $('#youtube_preview_text_btn').on 'click', ->
    source = $('#youtube_source').val()
    if source != ''
      youtube_preview(get_youtube_id(source))

  #검색결과 ROW 클릭 이벤트
  $('#youtube_search_result_div').on 'click', '.youtube_row',  ->
    $(@).children().children('.youtube_id').prop('checked', true)
    $('#youtube_hidden_id').val($(@).children().children('.youtube_id').val())
    $('#youtube_hidden_image_url').val($(@).children().children('.youtube_id').attr('url'))


  #검색결과 중 미리보기 이벤트
  $('#VideoModal').on 'click', '#youtube_preview_btn', ->

    $('#youtube_source').val('')

    id = $('#youtube_hidden_id').val()

    if id != ''
      youtube_preview(id)

  #모달 창 닫기 이벤트
  $('#VideoModal').on 'hide', ->
    $('#youtube_preview_div').children().remove()
    $('#youtube_body').children().remove()
    $('#youtube_query').val('')
    $('#youtube_hidden_id').val('')
    $('#youtube_source').val('')
    $('#youtube_page').val('1')




  #스크롤이벤트
  $('#youtube_search_result_div').on 'scroll', (e)  ->
    if ($('#youtube_search_result_div').outerHeight() > ($('#youtube_search_result_div').get(0).scrollHeight - $('#youtube_search_result_div').scrollTop() - 50  ))
      if $('#is_loading').val() == 'false'
      #e.stopImmediatePropagation()
        $('#youtube_page').val( parseInt($('#youtube_page').val()) + 1)
        $('#is_loading').val('true')
        $('#youtube_submit').click()
        activity_indicator_start()


  #유튜브 저장 버튼 이벤트
  $('#youtube_save_btn').on 'click', ->
    source = $('#youtube_source').val()
    id = $('#youtube_hidden_id').val()

    html = ''



    if source != '' && id != ''

      html += template(get_youtube_id(source))
      $('#image_upload_thumb').append(html)

    else if source != '' || id == ''
      html += template(get_youtube_id(source))
      $('#image_upload_thumb').append(html)

    else if id != '' ||  source == ''
      html += template(id, $('#youtube_hidden_image_url').val())
#      $('#video_thumb').append(html)
      $('#image_upload_thumb').append(html)

#    $('#tab_videos').click()
    $('#youtube_close_btn').click()
    $('.nano').nanoScroller()

  #유튜브 저장 템플릿
  template = (id, url) ->

#    last_count = parseInt($('#last_count').val())
    last_count = $('#image_upload_thumb li').length + 1;

    html = ''
    html += '<li class="video_upload thumbnail_content">'
    html += '<div class="background_play" ></div>'
    html += '<div style="display: none" class="thumbnails_icon">'
    html += '<div class="icon_move"><i class="icon-move"></i></div>'

    html += '<div class="icon_remove"><i class="icon-remove-sign"></i></div>'
    html += '</div>'



    if !url
      html += ' <img src="http://img.youtube.com/vi/' + id + '/1.jpg" style="width:140px; height:110px;" class="register_thumb">'
    else
      html += ' <img src="' + url + '" style="width:140px; height:110px;" class="register_thumb">'


    html += '<input type="hidden" class="caption" name="showcase[contents_attributes][' + last_count + '][caption]" value="" >'
#    html += '<input type="hidden" class="showcase_image_id" name="showcase[contents_attributes][' + last_count + '][showcase_image_id]" value="" >'
    html += '<input type="hidden" class="source_id" name="showcase[contents_attributes][' + last_count + '][source_id]" value="' + id + '" >'
    html += '<input type="hidden" class="source_type" name="showcase[contents_attributes][' + last_count + '][source_type]" value="youtube" >'
    html += '<input type="hidden" class="url" name="showcase[contents_attributes][' + last_count + '][url]" value="" >'
    html += '</li>'

#    last_count = last_count + 1
#    $('#last_count').val(last_count)

    return html

  #유튜브 미리보기 함수
  youtube_preview = (id) ->

    html = '<iframe width="387" height="260" src="http://www.youtube.com/embed/' + id + '" frameborder="0" allowfullscreen></iframe>'
    $('#youtube_preview_div').html(html)

  #유튜브 소스에서 아이디 추출 함수
  get_youtube_id = (source) ->
    if source != ''
      pattern = new RegExp('(?:https?://)?(?:www\\.)?(?:youtu\\.be/|youtube\\.com(?:/embed/|/v/|/watch\\?v=))([\\w-]{10,12})', 'g')
      result = pattern.exec(source)

      if result != null
        return result[1]
      else
        return ''
    else
      return ''


  activity_indicator_start = ->
    $('#spin').show().spin(
      lines: 10
      length: 5
      width: 3
      radius: 5
      color: 'white'
      speed: 1
      trail: 60
      shadow: false
    )

  activity_indicator_stop = ->
    $('#spin').hide().spin(false)




