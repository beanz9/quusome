# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('#showcase_comment_submit_btn').on 'click', ->
    if $('#comment_text').val() == ''
      alert("댓글을 입력하세요.")
      return

    $('#showcase_comment_submit').click()
    $('#comment_text').val('')


  $('form#comment_form').bind "ajax:error", (e, data, status, xhr) ->

    if data.status == 406
      alert("로그인이 필요합니다.")
      location.href = "/users/sign_in"
