# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->




  $('.tab-content').on 'click', '.accept_true', ->
    if confirm("요청수락을 하시겠습니까?")
      $(@).siblings('form').find('div > .accept_true_submit').click()

  $('.tab-content').on 'click', '.accept_false', ->
    if confirm("요청을 거절 하시겠습니까?")
      $(@).siblings('form').find('div > .accept_false_submit').click()

  $('.tab-content').on 'click', '.agent_contract_user', ->
    showcase_id = $($(@).children('.agent_contract_showcase_id')).text()
    html = "<img src='" + $($(@).children('a').children('img')).attr('src') + "' style='width:48px; height:48px'>"
    $('#agent_contract_user_image_' + showcase_id).html(html)

    $('#agent_contract_user_id_' + showcase_id).html($($(@).children('.agent_contract_user_id')).text())
    $('#target_user_id_' + showcase_id).attr('value', $($(@).children('.agent_contract_user')).text())

    $('#agent_contract_remain_date_' + showcase_id).html($($(@).children('.agent_contract_remain_date')).text() + "일")

  $('.tab-content').on 'click', '.message_submit_btn', ->


#    if $(@).siblings('.message_body').val() == ''
#      alert("메세지를 입력하세요.")
#      return
#
#    else if $(@).val() != '' && $('#agent_contract_user_id_' + $(@).val()).text() == ''
#      alert("에이전트를 선택하세요.")
#      return
#
#    $(@).siblings('.message_submit').click()
#    $(@).siblings('.message_body').val("")

  $(".operating_showcase_image").imgLiquid(
                                           fill: true
                                           fadeInTime: 200
                                           horizontalAlign: "center"
                                           verticalAlign: "middle"
                                           )

  $(".joining_showcase_image").imgLiquid(
                                         fill: true
                                         fadeInTime: 200
                                         horizontalAlign: "center"
                                         verticalAlign: "middle"
                                         )

  $(".favorite_showcase_image").imgLiquid(
                                          fill: true
                                          fadeInTime: 200
                                          horizontalAlign: "center"
                                          verticalAlign: "middle"
                                          )

  $('.agent_request_image').imgLiquid(
                                      fill: true
                                      fadeInTime: 200
                                      horizontalAlign: "center"
                                      verticalAlign: "middle"
                                      )

  #프로필 모달
  $('a[data-target=#ProfileModal]').on 'click', (e) ->
    e.preventDefault()
    e.stopPropagation()
    $('body').modalmanager('loading')
    $.rails.handleRemote($(@))

  #에이전트 모달
  $('a[data-target=#MyAgentModal]').on 'click', (e) ->
    e.preventDefault()
    e.stopPropagation()
    $('body').modalmanager('loading')
    $.rails.handleRemote($(@))

  $('.user_info_reward_icon').tooltip(
    html: true

  )

  $('.operating_showcase_image').hover ->
    $(@).find('.operating_showcase_btn').fadeToggle(200)




#  $('.user_info_reward_icon').tooltip('show')


#  window.wiselinks = new Wiselinks('#tab_content', html4: true);
#
#  $(document).off('page:loading').on(
#    'page:loading'
#    (event, $target, render, url) ->
#      console.log("Loading: #{url} to #{$target.selector} within '#{render}'")
#    # code to start loading animation
#  )
#
#  $(document).off('page:redirected').on(
#    'page:redirected'
#    (event, $target, render, url) ->
#      console.log("Redirected to: #{url}")
#    # code to start loading animation
#  )
#
#  $(document).off('page:always').on(
#    'page:always'
#    (event, xhr, settings) ->
#      console.log("Wiselinks page loading completed")
#
#      $('.agent_request_image').imgLiquid(
#                                          fill: true
#                                          fadeInTime: 200
#                                          horizontalAlign: "center"
#                                          verticalAlign: "middle"
#                                          );
#    # code to stop loading animation
#  )
#
#  $(document).off('page:done').on(
#    'page:done'
#    (event, $target, status, url, data) ->
#      console.log("Wiselinks status: '#{status}'")
#  )
#
#  $(document).off('page:fail').on(
#    'page:fail'
#    (event, $target, status, url, error) ->
#      console.log("Wiselinks status: '#{status}'")
#    # code to show error message
#  )







