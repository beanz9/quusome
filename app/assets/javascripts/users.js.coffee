# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  #$('#birth_day').datepicker(
  #  autoclose: true
  #)

  $('#company_main_products').tagit()

  $('#user_user_id').on blur: ->
    email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    $('#user_id_ok').css("display", "none")

    if $('#user_user_id').val() != ''


      $('#user_id_spin').spin(
        lines: 10
        length: 4
        width: 3
        radius: 3
        top: -26
        left: 295
      )

      $.ajax '/user_id_check',
        type: 'get'
        data: 'user_id=' + $('#user_user_id').val()
        dataType: 'json'
        success: (data, textStatus, jqXHR) ->
          $('#user_id_spin').spin(false)
          if Number(data.count) > 0
            alert("중복된 아이디가 있습니다.\n다른 아이디를 입력해 주세요.")
            $('#user_user_id').val('')
            $('#user_user_id').focus()
          else
            $('#user_id_ok').css("display", "inline-block")

        error: (jqXHR, textStatus, errorThrown) ->
          $('#user_id_spin').spin(false)
          alert("에러가 발생했습니다.")




  $('#user_email').on blur: ->

    email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    $('#email_ok').css("display", "none")

    if $('#user_email').val() != ''
      if !email_filter.test($('#user_email').val())
        alert("이메일 형식을 입력하세요.")
        $('#user_email').focus()
      else


        $('#email_spin').spin(
          lines: 10
          length: 4
          width: 3
          radius: 3
          top: -26
          left: 295
        )

        $.ajax '/email_check',
          type: 'get'
          data: 'email=' + $('#user_email').val()
          dataType: 'json'
          success: (data, textStatus, jqXHR) ->
            $('#email_spin').spin(false)
            if Number(data.count) > 0
              alert("중복된 이메일이 있습니다.\n다른 이메일을 입력해 주세요.")
              $('#user_email').val('')
              $('#user_email').focus()
            else
              $('#email_ok').css("display", "inline-block")

          error: (jqXHR, textStatus, errorThrown) ->
            $('#email_spin').spin(false)
            alert("에러가 발생했습니다.")

  $('#user_password_confirmation').on blur: ->

    $('#user_password_ok').css("display", "none")
    $('#user_password_confirmation_ok').css("display", "none")

    if $('#user_password_confirmation').val() != ''
      if $('#user_password').val() == $('#user_password_confirmation').val()
        $('#user_password_ok').css("display", "inline-block")
        $('#user_password_confirmation_ok').css("display", "inline-block")
      else

        alert("비밀번호가 일치하지 않습니다.")


    $('#user_password').on blur: ->

      $('#user_password_ok').css("display", "none")
      $('#user_password_confirmation_ok').css("display", "none")

      if ('#user_password').val() != ''

        if $('#user_password').val() == $('#user_password_confirmation').val()
          $('#user_password_ok').css("display", "inline-block")
          $('#user_password_confirmation_ok').css("display", "inline-block")
        else

          alert("비밀번호가 일치하지 않습니다.")



  $('#submit_btn').on click: ->

    email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    url_filter = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;


    if $('#user_email').val() == ''
      alert("이메일을 입력하세요.")
      $('#user_email').focus()
      return
    else
      if !email_filter.test($('#user_email').val())
        alert("이메일 형식을 입력하세요.")
        $('#user_email').focus()
        return

    if $('#user_user_id').val() == ''
      alert("아이디를 입력하세요.")
      $('#user_user_id').focus();
      return

    if $('#user_password').val() == ''
      alert("비밀번호를 입력하세요.")
      $('#user_password').focus()
      return

    if $('#user_password').val() != $('#user_password_confirmation').val()
      alert("비밀번호가 일치하지 않습니다.")
      $('#user_password').focus()
      return

    if $('#user_name').val() == ''
      alert("이름을 입력하세요.")
      $('#user_name').focus()
      return

    if $('#user_country').val() == ''
      alert("지역을 선택하세요.")
      $('#user_country').focus()
      return

    if $('#user_birth_year').val() == ''
      alert("생년월일을 입력하세요.")
      $('#user_birth_year').focus()
      return

    if $('#user_birth_month').val() == ''
      alert("생년월일을 입력하세요.")
      $('#user_birth_month').focus()
      return

    if $('#user_birth_day').val() == ''
      alert("생년월일을 입력하세요.")
      $('#user_birth_day').focus()
      return

#    if $('#user_secondary_email').val() != ''
#      if !email_filter.test($('#user_secondary_email').val())
#        alert("이메일 형식을 입력하세요.")
#        $('#user_secondary_email').focus()
#        return

    if $('#user_company_homepage').val() != ''
      if !url_filter.test($('#user_company_homepage').val())
        alert("URL 주소 형식을 입력하세요.")
        $('#user_company_homepage').focus()
        return

    $('#company_main_product_list').val($('#company_main_products').tagit('assignedTags'))
    $('#user_create_submit').click()


  $('#update_btn').on click: ->

    email_filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    url_filter = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;

#    if $('#user_name').val() == ''
#      alert("이름을 입력하세요.")
#      $('#user_name').focus()
#      return
#
#    if $('#user_country').val() == ''
#      alert("지역을 선택하세요.")
#      $('#user_country').focus()
#      return
#
#    if $('#user_birth_year').val() == ''
#      alert("생년월일을 입력하세요.")
#      $('#user_birth_year').focus()
#      return
#
#    if $('#user_birth_month').val() == ''
#      alert("생년월일을 입력하세요.")
#      $('#user_birth_month').focus()
#      return
#
#    if $('#user_birth_day').val() == ''
#      alert("생년월일을 입력하세요.")
#      $('#user_birth_day').focus()
#      return



    if $('#user_company_homepage').val() != ''
      if !url_filter.test($('#user_company_homepage').val())
        alert("URL 주소 형식을 입력하세요.")
        $('#user_company_homepage').focus()
        return

    $('#company_main_product_list').val($('#company_main_products').tagit('assignedTags'))
    $('#user_update_submit').click()



  $('#user_info_tab li a').on 'click', ->
    $('#user_info_tab li').removeClass('active')
    $(@).parent('li').addClass('active')


  $('#password_update_btn').on 'click', ->

    if $('#new_password').val() == ""
      alert("새 비밀번호를 입력하세요.")
      return
    else if $('#new_password_confirm').val() == ""
      alert("새 비밀번호를 확인을 입력하세요.")
      return
    else if $('#new_password').val() != $('#new_password_confirm').val()
      alert("새 비밀번호가 일치하지 않습니다.")
      return;
    else if $('#current_password').val() == ""
      alert("현재 비밀번호를 입력하세요.")
      return



    $('#password_update_submit').click()

  $('#change_password_btn').on 'click', ->

    if $('#new_password').val() == ""
      alert("새 비밀번호를 입력하세요.")
      return
    else if $('#new_password_confirmation').val() == ""
      alert("새 비밀번호 확인을 입력하세요.")
      return
    else if $('#new_password').val() != $('#new_password_confirmation').val()
      alert("새 비밀번호가 일치하지 않습니다.")
      return

    $('#change_password_submit').click()



















