class AboutController < ApplicationController

  def index

    @about = About.last

    respond_to do |format|
      format.html
    end
  end

  def agreement_of_utilization

    @agreement = Agreement.last

    respond_to do |format|
      format.html
    end
  end

  def privacy_policy

    @privacy_policy = PrivacyPolicy.last

    respond_to do |format|
      format.html
    end
  end

  def partnership_suggestion


    respond_to do |format|
      format.html
    end
  end
end