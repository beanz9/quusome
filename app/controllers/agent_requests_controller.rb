

class AgentRequestsController < ApplicationController
  before_filter :store_location
  before_filter :authenticate_user!, :only => [:create]

  def create
    @user = User.find(params[:user_id])

    @receipt = current_user.send_message(@user, 'agent_request', 'message')

    if @receipt
      notification = Notification.find(@receipt.notification_id)
      notification.update_attributes(:message_type => 'agent_request', :showcase_id => params[:showcase_id])

      #메일발송
      NotificationMailer.agent_request(current_user, @user, params[:showcase_id], @receipt.created_at).deliver
    end
    flash[:success] = true

  rescue
    flash[:success] = false

  end
end
