class ApplicationController < ActionController::Base
  protect_from_forgery
  include PublicActivity::StoreController

  before_filter :set_locale, :get_host_ip


  private

  def store_location
    session[:return_to] ||= request.referer
  end



  # set the language
  def set_locale
    if params[:locale].blank?
      I18n.locale = extract_locale_from_accept_language_header
    else
      I18n.locale = params[:locale]
    end

  end

  # pass in language as a default url parameter
  #def default_url_options(options = {})
  #  {locale: I18n.locale}
  #end

  # extract the language from the clients browser
  def extract_locale_from_accept_language_header
    browser_locale = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first


    if I18n.available_locales.include? browser_locale.to_sym
      browser_locale
    else
      I18n.default_locale
    end
  end

  def after_sign_in_path_for(resource)

    resource.sign_in_count <= 1 ? '/active_sign_up' : root_path

  end

  def after_sign_out_path_for(resource)

    '/'
  end

  def get_host_ip
    #p request.host
    #p request.host
    #p request.host
    #p request.host_with_port
    #p request.host_with_port
  end

end
