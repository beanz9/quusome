class BuySlotsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @product_items = ProductItem.order("cast(product_price as integer)").all
    @purchased_product = PurchasedProduct.enable_product(current_user.id).first

    unless @purchased_product.nil?
      used_slots = UsedSlot.where(:purchased_product_id => @purchased_product.id)

      #사용가능한 슬롯
      if @purchased_product.product_item.slot_count == 0
        @enable_slots = nil
      else
        @enable_slots = @purchased_product.product_item.slot_count - used_slots.count
      end



      #사용한 슬롯
      @used_slots = current_user.agent_contracts_by_showcase_user.where("end_date >= ?", Time.now).count
    end

    respond_to do |format|
      format.html
    end
  end

  def purchase_product

    @purchase_product = PurchasedProduct.new({
      :product_item_id => params[:product_item_id],
      :user_id => current_user.id,
      :start_date => Time.now,
      :end_date => Time.now + 1.month
    })

    if @purchase_product.save
      flash[:success] = true
    end

  rescue
    flash[:success] = false
  end
end
