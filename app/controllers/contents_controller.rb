class ContentsController < ApplicationController

  def destroy

    @content = Content.find(params[:id])

    if @content.source_type == 'upload'

      @showcase_image = ShowcaseImage.find(@content.showcase_image_id)
      @showcase_image.destroy

    end

    @content.destroy

    head :ok
  end
end