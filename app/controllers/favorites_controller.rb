class FavoritesController < ApplicationController
  before_filter :store_location
  before_filter :authenticate_user!, :only => [:create]


  def create
    @showcase = Showcase.find(params[:showcase_id])



    current_user.favorite @showcase
  end

  private



end
