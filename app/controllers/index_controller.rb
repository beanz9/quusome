class IndexController < ApplicationController

  layout false

  def index
    if current_user
      redirect_to '/showcases'
    end
  end

  def sign_up
    @user = User.new
  end
end
