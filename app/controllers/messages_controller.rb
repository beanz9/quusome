class MessagesController < ApplicationController

  before_filter :store_location
  before_filter :authenticate_user!, :only => [:create]

  def create

    #메세지 받는 사람 조회
    @user = User.find(params[:user_id])
    #메세지 보내기
    @receipt = current_user.send_message(@user, params[:message_body], 'message')

    if @receipt
      notification = Notification.find(@receipt.notification_id)
      notification.update_attributes(:message_type => 'message', :target_user_id => @user.id)
      ConversationUser.create({:user_id => current_user.id, :notification_id => notification.id})
      ConversationUser.create({:user_id => @user.id, :notification_id => notification.id})
    end
    flash[:success] = true

  rescue
    flash[:success] = false

  end

end
