class Mobiles::ShowcasesController < ApplicationController

  layout 'mobile/application'

  def index

    if params[:q].present?
      @showcases =  Showcase.publish.search(params[:q]).order('created_at desc').page(params[:page]).per(16)
    else
      @showcases = Showcase.publish.order('created_at desc').page(params[:page]).per(16)
    end

    respond_to do |format|
      format.html
    end
  end

  def show

    @showcase = Showcase.find(params[:id])

    p @showcase
    p ">>>>>>>>"

    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    p "new"
  end
end