# encoding: UTF-8

class RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def create
    build_resource
    #resource.set_tag_list_on(:company_main_products, params[:company_main_product_list])

    if resource.save


      if resource.active_for_authentication?


        #params[:selected_interest_areas].try(:each) do |area|
        #  SelectedInterestArea.create(:user_id => resource.id, :interest_area_id => area)
        #end



        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords resource
      respond_with resource
    end
  end

  def edit
    super
  end

  def update
    super
  end



  def destroy
    @working_agent_count = current_user.agent_contracts_by_showcase_user.working_agent.count

    @joining_agent_count = current_user.agent_contracts.working_agent.count


    if @working_agent_count > 0
      flash[:working_agent] = "현재 계약 중인 에이전트가 있습니다. 계약기간이 끝난 후에 탈퇴가 가능합니다."
      redirect_to destroy_check_path
    elsif @joining_agent_count > 0
      flash[:joining_agent] = "현재 계약 중인 쇼케이스가 있습니다. 계약기간이 끝난 후에 탈퇴가 가능합니다."
      redirect_to destroy_check_path
    else
      resource.destroy
      #Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
      redirect_to destroy_complete_path
    end

  end

  def cancel
    super
  end

  protected

  def after_inactive_sign_up_path_for(resource)

    #announcement
    announcement = Announcement.new({
                                        :body => "큐썸에 오신걸 환영합니다. 프로필 정보가 많을수록 에이전트 신청이 수락될 가능성이 높아집니다. <a href='/users/#{resource.id}/edit'>프로필 추가입력</a>",
                                        :title => "추가정보",
                                        :announcement_type => "warning",
                                        :user_id => resource.id

                                    })

    announcement.save!

    "/inactive_sign_up"
  end

  def after_sign_up_path_for(resource)
    resource.sign_in_count <= 1 ? '/active_sign_up' : root_path
  end

  def after_update_path_for(resource)
    "/users/#{resource.id}/edit"
  end

end