class RewardItemsController < ApplicationController
  # GET /reward_items
  # GET /reward_items.json
  def index
    @reward_items = RewardItem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @reward_items }
    end
  end

  # GET /reward_items/1
  # GET /reward_items/1.json
  def show
    @reward_item = RewardItem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @reward_item }
    end
  end

  # GET /reward_items/new
  # GET /reward_items/new.json
  def new
    @reward_item = RewardItem.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @reward_item }
    end
  end

  # GET /reward_items/1/edit
  def edit
    @reward_item = RewardItem.find(params[:id])
  end

  # POST /reward_items
  # POST /reward_items.json
  def create
    @reward_item = RewardItem.new(params[:reward_item])

    respond_to do |format|
      if @reward_item.save
        format.html { redirect_to @reward_item, notice: 'Reward item was successfully created.' }
        format.json { render json: @reward_item, status: :created, location: @reward_item }
      else
        format.html { render action: "new" }
        format.json { render json: @reward_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /reward_items/1
  # PUT /reward_items/1.json
  def update
    @reward_item = RewardItem.find(params[:id])

    respond_to do |format|
      if @reward_item.update_attributes(params[:reward_item])
        format.html { redirect_to @reward_item, notice: 'Reward item was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @reward_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reward_items/1
  # DELETE /reward_items/1.json
  def destroy
    @reward_item = RewardItem.find(params[:id])
    @reward_item.destroy

    respond_to do |format|
      format.html { redirect_to reward_items_url }
      format.json { head :no_content }
    end
  end
end
