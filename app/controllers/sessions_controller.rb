class SessionsController < Devise::SessionsController
  def create
    resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    sign_in_and_redirect(resource_name, resource)
  end

  def sign_in_and_redirect(resource_or_scope, resource=nil)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    resource ||= resource_or_scope
    sign_in(scope, resource) unless warden.user(scope) == resource
    redirect_url  = session[:return_to] == nil ? '/showcases' : session[:return_to]

    return render :json => {:success => true, :redirect_url => redirect_url}
  end

  #def sign_in_and_redirect(resource_or_scope, resource=nil)
  #  scope      = Devise::Mapping.find_scope!(resource_or_scope)
  #  resource ||= resource_or_scope
  #  redirect_url = stored_location_for(scope)
  #  respond_to do |format|
  #     format.js do
  #      sign_in(scope, resource) unless warden.user(scope) == resource
  #      if redirect_url.present?
  #        redirect_url = "#{redirect_url}.js" unless redirect_url[-3..-1] == '.js'
  #        redirect_url += redirect_url.match(/\?/) ? '&' : '?'
  #        redirect_url += "after_authentication=true"
  #        redirect_to redirect_url
  #      else
  #        render(:update) do |page|
  #          page << render('/shared/after_authentication')
  #        end
  #      end
  #    end
  #    format.html { super }
  #  end
  #end

  def failure
    return render :json => {:success => false, :errors => ["Login failed."]}
  end
end