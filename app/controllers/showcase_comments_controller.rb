class ShowcaseCommentsController < ApplicationController
  before_filter :store_location
  before_filter :authenticate_user!, :only => :create

  def create
    @showcase = Showcase.find(params[:showcase_id])
    @comment = Comment.build_from(@showcase, current_user.id, params[:body])
    @comment.save

    @comment_threads = @showcase.comment_threads.order('created_at desc')

  end
end
