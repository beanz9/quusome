class ShowcaseImagesController < ApplicationController
  def create

    @images = Array.new

    params[:showcase_images].each do |showcase_image|

      if showcase_image[1][:type] == 'upload'
        unless showcase_image[1][:image].blank?
          image = ShowcaseImage.create({ image: showcase_image[1][:image] })

          image_object = OpenStruct.new
          image_object.source_type = 'upload'
          image_object.id = image.id
          image_object.url = image.image.url
          image_object.caption = showcase_image[1][:caption]
          image_object.link = showcase_image[1][:link]

          @images.push(image_object)
        end

      else
        unless showcase_image[1][:url].blank?
          image_object = OpenStruct.new
          image_object.source_type = 'url'
          image_object.id = ''
          image_object.url = showcase_image[1][:url]
          image_object.caption = showcase_image[1][:caption]
          image_object.link = showcase_image[1][:link]

          @images.push(image_object)
        end
      end
    end

    respond_to do |format|
      format.js
      format.json { render json: @showcases }
    end
  end

  def destroy

    @showcase_image = ShowcaseImage.find(params[:id])

    @showcase_image.destroy

    #respond_to do |format|
    #  format.js
    #  format.json { render json: @showcase_image }
    #end
  end
end
