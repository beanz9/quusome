# encoding: UTF-8

class ShowcasesController < ApplicationController

  before_filter :authenticate_user!, :except => [:index, :show]
  before_filter :user_filter, :only => [:edit, :destroy]

  impressionist :actions => [:show]
  # GET /showcases
  # GET /showcases.json
  def index

    if params[:q].present?
      @showcases =  Showcase.publish.search(params[:q]).order('created_at desc').page(params[:page]).per(16)
    else
      @showcases = Showcase.publish.order('created_at desc').page(params[:page]).per(16)
    end

    #@activities = PublicActivity::Activity.order("created_at DESC").all


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @showcases }
    end
  end

  # GET /showcases/1
  # GET /showcases/1.json
  def show


    @showcase = Showcase.find(params[:id])
    @comment_threads = @showcase.comment_threads.order('created_at desc')
    session[:return_to] = "/showcases/#{params[:id]}"

    respond_to do |format|
      if @showcase.is_publish == '1'
        format.html
      else
        if current_user and current_user.id == @showcase.created_by
          format.html
        else
          format.html { render :template => "showcases/show_private" }
        end


      end

    end
  end

  # GET /showcases/new
  # GET /showcases/new.json
  def new


    @showcase = Showcase.new
    @reward_items = RewardItem.where(:location => I18n.locale.to_s.upcase)


    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @showcase }
    end
  end

  # GET /showcases/1/edit
  def edit
    @showcase = Showcase.find(params[:id])
    @reward_items = RewardItem.all
    #ShowcaseUpdateWorker.perform_async(params[:id], current_user.id)
  end

  # POST /showcases
  # POST /showcases.json
  def create
    @showcase = Showcase.new(params[:showcase]) do |showcase|
      showcase.created_by = current_user.id
      showcase.updated_by = current_user.id
    end

    #notice = '쇼케이스가 성공적으로 저장되었습니다. 친구에게 초대메세지를 보내보세요.'
    respond_to do |format|
      if @showcase.save
        format.html { redirect_to @showcase, notice: notice }
        format.json { render json: @showcase, status: :created, location: @showcase }
      else
        format.html { render action: "new" }
        format.json { render json: @showcase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /showcases/1
  # PUT /showcases/1.json
  def update
    @showcase = Showcase.find(params[:id])

    @showcase.contents.destroy_all

    respond_to do |format|
      if @showcase.update_attributes(params[:showcase])
        format.html { redirect_to @showcase, notice: '쇼케이스가 성공적으로 수정되었습니다.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @showcase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /showcases/1
  # DELETE /showcases/1.json
  def destroy
    @showcase = Showcase.find(params[:id])

    if @showcase.agent_contracts.working_agent.count == 0

    #업로드된 이미지 삭제
      @showcase.contents.each do |content|
        if content.source_type == 'upload'
          showcase_image = ShowcaseImage.where(:id => content.showcase_image_id).first
          showcase_image.destroy if showcase_image.present?
        end
      end
      @showcase.destroy
      info = "쇼케이스가 성공적으로 삭제되었습니다."
      flash[:info] = info
    else
      warning = "계약된 에이전트가 있습니다. 계약이 끝난 후 삭제해주세요."
      flash[:warning] = warning
    end


    respond_to do |format|
      format.html { redirect_to user_info_operating_showcase_path(current_user) }
      format.json { head :no_content }
    end
  end

  def showcase_rewards_destroy
    @showcase_reward = ShowcaseReward.find(params[:id])
    @showcase_reward.destroy

    head :ok
  end

  def user_filter
    showcase = Showcase.find(params[:id])
    unless showcase.user == current_user
      redirect_to showcase, notice: "수정이나 삭제 권한이 없습니다."
    end

  end
end
