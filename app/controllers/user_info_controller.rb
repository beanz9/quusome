class UserInfoController < ApplicationController

  before_filter :authenticate_user!, :except => [:user_info_modal]
  before_filter :user_object, :except => [:agent_request_accept, :user_info_modal]

  def index

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @user }
    end

  end

  def basic_info
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @user }
    end
  end

  def agent_request
    @conversations = @user.mailbox.inbox_agent_request.page(params[:page]).per(10)


  end

  def favorite_showcase
    @favorite_items = @user.favorite_items.page(params[:page]).per(6)
  end

  def joining_showcase

    if params[:q] == 'processing'
      @agent_contracts = @user.agent_contracts.working_agent.order("created_at DESC").page(params[:page]).per(2)
    elsif params[:q] == 'end'
      @agent_contracts = @user.agent_contracts.end_agent.order("created_at DESC").page(params[:page]).per(2)
    else

      @agent_contracts = @user.agent_contracts.order("created_at DESC").page(params[:page]).per(2)
    end


  end

  def message

    conversation_user

    @target_user_id = @conversation_users.first.user_id if @conversation_users.present?

    conversation

  end

  def message_send
    #메세지 받는 사람 조회
    target_user = User.find(params[:target_user_id])
    #메세지 보내기
    @receipt = current_user.send_message(target_user, params[:message_body], 'message')

    if @receipt
      notification = Notification.find(@receipt.notification_id)
      notification.update_attributes(:message_type => 'message', :target_user_id => target_user.id)
      ConversationUser.create({:user_id => current_user.id, :notification_id => notification.id})
      ConversationUser.create({:user_id => target_user.id, :notification_id => notification.id})

      conversation
    end

    flash[:success] = true



  end

  def conversation_user


      #if Rails.env.development?
      #  @conversation_users = Notification.includes(:conversation_user).
      #      order("notifications.created_at DESC").
      #      where("(notifications.sender_id = :user_id OR notifications.target_user_id = :user_id) and conversation_users.user_id <> :user_id", :user_id => @user.id).
      #      group("conversation_users.user_id").page(params[:page]).per(5)
      #else

        @notifications = Notification.select(:id).where("sender_id = :user_id or target_user_id = :user_id", :user_id => @user.id)

        @conversation_users = ConversationUser.select("distinct(user_id) as user_id, max(created_at)").
            where("user_id <> :user_id and notification_id in (:notification_ids)",
                                                                 :user_id => @user.id, :notification_ids => @notifications).
            order("max(created_at) DESC").
            group("user_id").
            page(params[:page]).per(5)




      #end


  end

  def conversation

    if @conversation_users.nil?
      @target_user_id = params[:target_user_id]
    else
      @target_user_id = @conversation_users.first.user_id if @conversation_users.present?
    end
    #@conversations = Notification.order('notifications.created_at DESC').joins(:receipts).
    #    where('receipts.receiver_id = ? AND notifications.message_type = ? AND (notifications.sender_id = ? OR notifications.sender_id = ?) AND (notifications.target_user_id = ? OR notifications.target_user_id = ?)',
    #          @user.id, 'message', @user.id, @target_user_id, @user.id, @target_user_id).page(params[:page]).per(7)

    @conversations = Notification.includes(:conversation_user).
        order("notifications.created_at DESC").
        where("(notifications.sender_id = :user_id OR notifications.target_user_id = :user_id) and conversation_users.user_id = :target_user_id",
              :user_id => @user.id, :target_user_id => @target_user_id).page(params[:page]).per(5)

  end

  def operating_showcase

    if params[:q] == 'public'
      @showcases = @user.showcases.publish.order("updated_at DESC").page(params[:page]).per(6)
    elsif params[:q] == 'private'
      @showcases = @user.showcases.not_publish.order("updated_at DESC").page(params[:page]).per(6)
    else
      @showcases = @user.showcases.order("updated_at DESC").page(params[:page]).per(6)
    end


  end

  #에이전트 요청 수락/거절

  def agent_request_accept

    ActiveRecord::Base.transaction do

      #purchased_product = PurchasedProduct.enable_product(current_user.id).first
      #used_slots = UsedSlot.where(:purchased_product_id => purchased_product.id)

      #enable_slots = (purchased_product.product_item.slot_count).to_i - used_slots.count

      @notification = Notification.find(params[:id])
      @user = User.find(@notification.sender_id)

      if params[:is_accept] == "true"

        purchased_product = PurchasedProduct.enable_product(current_user.id).first


        if purchased_product.nil?
          @enable_slots = 0
        else
          used_slots = UsedSlot.where(:purchased_product_id => purchased_product.id)

          if purchased_product.product_item.slot_count == 0
            #무제한
            @enable_slots = 1000000
          else
            @enable_slots = purchased_product.product_item.slot_count - used_slots.count
          end

        end


        if @enable_slots > 0
          @notification.update_attributes(:is_accept => true)
          @receipt = current_user.send_message(@user, 'agent_result', "true")
        end

      else
        @notification.update_attributes(:is_accept => false)
        @receipt = current_user.send_message(@user, 'agent_result', "false")
      end

      if !@receipt.nil?
        notification = Notification.find(@receipt.notification_id)
        notification.update_attributes(:message_type => 'agent_request', :showcase_id => @notification.showcase_id)

        if params[:is_accept] == "true"



          #사용가능한 슬롯 조회 후 슬롯 할당

          if @enable_slots > 0
            showcase = Showcase.find(@notification.showcase_id)
            now = Time.now
            end_date = now + (showcase.mission_term.to_i).days

            AgentContract.new({
                :agent_user_id => @user.id,
                :showcase_id => showcase.id,
                :start_date => now,
                :end_date => end_date,
                :user_id => current_user.id

            }).save


            #메일발송
            NotificationMailer.agent_request_accept(current_user, @user, showcase.id,
                                                    @receipt.created_at, end_date).deliver


            flash[:enable_slot] = true

          else
            flash[:enable_slot] = false
          end
        end
      end
    end


    flash[:success] = true


  end


  def user_object
    @user = User.find(params[:id])
  end


  def user_info_modal

    @user = User.find(params[:id])

    respond_to do |format|
      format.html { redirect_to root_path } #for my controller, i wanted it to be JS only
      format.js
    end
  end

  def user_info_agent_modal


    @showcase = @user.showcases.unscoped.find(params[:showcase_id])

    @showcase_agents = @showcase.agent_contracts.order("created_at desc").page(params[:page]).per(12)

    #@showcases = Showcase.page(params[:page]).per(14)

    respond_to do |format|
      format.html { redirect_to root_path } #for my controller, i wanted it to be JS only
      format.js
    end
  end

  def user_info_agent_search

    @showcase = @user.showcases.unscoped.find(params[:showcase_id])

    if params[:q] == "processing"
      @showcase_agents = @showcase.agent_contracts.order("created_at desc").working_agent.page(params[:page]).per(12)
    elsif params[:q] == "end"
      @showcase_agents = @showcase.agent_contracts.order("created_at desc").end_agent.page(params[:page]).per(12)
    else
      @showcase_agents = @showcase.agent_contracts.order("created_at desc").page(params[:page]).per(12)
    end

    respond_to do |format|
      format.html { redirect_to root_path } #for my controller, i wanted it to be JS only
      format.js
    end
  end

end
