# encoding: UTF-8

class UsersController < ApplicationController


  # GET /users
  # GET /users.json
  def index
    #@users = User.all

    #respond_to do |format|
    #  format.html # index.html.erb
    #  format.json { render json: @users }
    #end

    redirect_to "/users/#{current_user.id}/user_info"
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])

  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])
    @user.set_tag_list_on(:company_main_products, params[:company_main_product_list])

    respond_to do |format|
      if @user.update_attributes(params[:user])


        format.html { redirect_to "/users/#{@user.id}/edit", notice: '회원정보가 성공적으로 수정되었습니다.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def destroy_check

    respond_to do |format|
      format.html

    end
  end

  def destroy_complete
    respond_to do |format|
      format.html

    end
  end


  def id_and_email_check
    email_count = User.where(:email => params[:email]).count
    id_count = User.where(:user_id => params[:user_id]).count

    respond_to do |format|
      format.json { render json: { email_count: email_count, id_count: id_count }}
    end
  end

  def email_check

    sleep(1.second)
    email_count = User.where(:email => params[:email]).count
    p email_count

    respond_to do |format|
        format.json { render json: { count: email_count } }
    end
  end

  def user_id_check

    sleep(1.second)
    user_id_count = User.where(:user_id => params[:user_id]).count

    respond_to do |format|
      format.json { render json: { count: user_id_count }}
    end
  end

  def wizard
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  def inactive_sign_up

  end

  def active_sign_up

  end
end
