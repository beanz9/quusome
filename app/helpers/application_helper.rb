module ApplicationHelper

  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable(object, object_type)
    if object
      link_to object_type.downcase, object
    else
      "a #{object_type.downcase} which does not exist anymore"
    end
  end

  # Shortcut for outputing proper ownership of objects,
  # depending on who is looking
  def whose?(user, object)
    case object
      when Showcase
        owner = object.author
      when Comment
        owner = object.user
      else
        owner = nil
    end
    if user and owner
      if user.id == owner.id
        "his"
      else
        "#{owner.nickname}'s"
      end
    else
      ""
    end
  end

  def content_image_tag(content, style = {})

    if content.source_type == 'youtube'
      image_tag "http://img.youtube.com/vi/#{content.source_id}/0.jpg", :style => style
    else
      image_tag content.url, :style => style
    end

  end

  def profile_image_tag(url, style)

    if url.nil?
      image_tag "48x48/user48x48.jpg", :style => style
    else
      image_tag url, :style => style
    end
  end

  def reward_item_tag(showcase_reward, size="48", style)
    content = content_tag 'a',
                :class => "btn example btn-large showcase_reward",
                "rel" => "clickover",
                "data-placement" => "left",
                "data-id" => "##{dom_id(showcase_reward)}",
                "data-original-title" => showcase_reward.reward_item.title,
                :style => style do

      image_tag "#{size}x#{size}/#{showcase_reward.reward_item.image}"
    end

    content += content_tag 'div',
                :id => dom_id(showcase_reward),
                :style => "display: none;" do
      simple_format showcase_reward.detail_info
    end

    content

  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
