module DeviseHelper
  def devise_error_messages!
    return "" if resource.errors.empty?

    p  resource.errors

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
                      :count => resource.errors.count,
                      :resource => resource.class.model_name.human.downcase)

    p messages

    html = <<-HTML
      <div class="alert alert-error">
        <button type="button" class="close" data-dismiss="alert">x</button>
        #{sentence}
      </div>
    HTML

    html.html_safe
  end

  def devise_error_messages?
    resource.errors.empty? ? false : true
  end

end