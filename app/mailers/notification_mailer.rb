# encoding: UTF-8

class NotificationMailer < ActionMailer::Base
  default from: "admin@quusome.com"

  def agent_request(user, target_user, showcase_id, created_at)
    @user = user
    @target_user = target_user
    #@target_user = "van5150@naver.com"
    @showcase = Showcase.find(showcase_id)
    @created_at = created_at

    mail(to: @target_user.email, subject: 'Quusome.com 의 에이전트 요청 메세지 입니다.')

  end

  def agent_request_accept(user, target_user, showcase_id, created_at, end_date)
    @user = user
    @target_user = target_user
    @showcase = Showcase.find(showcase_id)
    @created_at = created_at
    @end_date = end_date

    mail(to: @target_user.email, subject: 'Quusome.com 의 에이전트 요청수락 메세지 입니다.')
  end

end
