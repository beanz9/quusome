# encoding: UTF-8

class UserMailer < Devise::Mailer
  helper :application

  private



  def headers_for(action, opts)

    if action == :confirmation_instructions

      subject = "환영합니다. #{resource.email} 님, Quusome.com 가입확인 메일입니다."

    elsif action == :reset_password_instructions
      subject =  "안녕하세요.  #{resource.email} 님, Quusome.com 의 비밀번호를 변경해주세요."

    else
      subject = "Welcome  #{resource.email}, unlock your Qitch.com account"

    end


    headers = {
        :subject       => subject,
        :to            => resource.email,
        :from          => mailer_sender(devise_mapping),
        :reply_to      => mailer_reply_to(devise_mapping),
        :template_path => template_paths,
        :template_name => action
    }.merge(opts)

    if resource.respond_to?(:headers_for)
      ActiveSupport::Deprecation.warn "Calling headers_for in the model is no longer supported. " <<
                                          "Please customize your mailer instead."
      headers.merge!(resource.headers_for(action))
    end

    @email = headers[:to]
    headers
  end


  #def confirmation_instructions(record, opts={})
  #
  #  headers["subject"] = "환영합니다. #{record.name} 님, Quusome.com 가입확인 메일입니다."
  #  super
  #end
  #
  #def reset_password_instructions(record, opts={})
  #  headers = {
  #      :subject => "Welcome  #{record.name}, reset your Qitch.com password"
  #  }
  #  super
  #end
  #
  #def unlock_instructions(record, opts={})
  #  headers = {
  #      :subject => "Welcome  #{record.name}, unlock your Qitch.com account"
  #  }
  #  super
  #end

  # you can then put any of your own methods here
end