class AgentContract < ActiveRecord::Base

  acts_as_paranoid

  attr_accessible :agent_user_id, :end_date, :showcase_id, :start_date, :user_id
  after_save :save_used_slot
  scope :working_agent, lambda { where("end_date >= ?", Time.now) }
  scope :end_agent, lambda { where("end_date < ?", Time.now) }

  belongs_to :user, :foreign_key => "agent_user_id"
  belongs_to :showcase_user, :class_name => 'User', :foreign_key => 'user_id'

  belongs_to :showcase
  has_one :used_slot

  attr_accessor :current_user


  private

  def save_used_slot
    purchased_product = PurchasedProduct.enable_product(self.user_id).first

    UsedSlot.new({
      :agent_contract_id => self.id,
      :purchased_product_id => purchased_product.id
    }).save

  end

end
