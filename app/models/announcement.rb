class Announcement < ActiveRecord::Base

  acts_as_paranoid

  belongs_to :user

  attr_accessible :body, :expire_date, :is_public, :is_read, :title, :announcement_type, :user_id

  scope :public, where(is_public: true)
  scope :private, where(is_public: false)
  scope :info_type, where(announcement_type: "info")
  scope :warning_type, where(announcement_type: "warning")
  scope :not_expire, where("expire_date >= ? or expire_date is null", Time.now)
  scope :expire, where("expire_date < ?", Time.now)
  scope :read, where(is_read: true)
  scope :unread, where(is_read: false)

  scope :get, lambda { |user_id| where("user_id = ? or user_id is null", user_id).not_expire }

end
