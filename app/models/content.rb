class Content < ActiveRecord::Base

  acts_as_paranoid

  attr_accessible :caption, :deleted_at, :order, :showcase_id, :showcase_image_id,
                  :source_type, :url, :source_id, :link
  belongs_to :showcase
end
