class ConversationUser < ActiveRecord::Base

  acts_as_paranoid



  attr_accessible :notification_id, :user_id

  belongs_to :user
  belongs_to :notification



end
