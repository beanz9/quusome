class FavoriteItem < ActiveRecord::Base

  acts_as_paranoid

  attr_accessible :showcase_id, :user_id

  belongs_to :showcase
  belongs_to :user
end
