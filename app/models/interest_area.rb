class InterestArea < ActiveRecord::Base
  attr_accessible :desc, :title, :selected_interest_areas_attributes, :users_attributes

  has_many :selected_interest_areas
  has_many :users, :through => :selected_interest_areas

end
