class ProductItem < ActiveRecord::Base
  attr_accessible :product_info, :product_name, :product_price, :product_color, :slot_count

  has_many :purchased_products
end
