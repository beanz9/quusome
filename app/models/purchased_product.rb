class PurchasedProduct < ActiveRecord::Base

  acts_as_paranoid

  attr_accessible :end_date, :product_item_id, :start_date, :user_id
  scope :enable_product, lambda { |user_id| where("user_id = ? and end_date >= ?", user_id, Time.now).order("end_date DESC")}

  belongs_to :user
  belongs_to :product_item

  def enable_slot_count
    self.product_item.slot_count
  end
end
