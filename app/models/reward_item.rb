class RewardItem < ActiveRecord::Base
  attr_accessible :description, :title, :image

  has_many :showcase_rewards
  has_many :showcases, :through => :showcase_rewards


end
