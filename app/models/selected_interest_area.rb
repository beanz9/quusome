class SelectedInterestArea < ActiveRecord::Base

  acts_as_paranoid

  attr_accessible :interest_area_id, :user_id

  belongs_to :user, :dependent => :destroy
  belongs_to :interest_area

end
