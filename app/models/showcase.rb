class Showcase < ActiveRecord::Base

  include PublicActivity::Model
  include PgSearch

  #default_scope where("is_publish = ?", "1")
  scope :not_publish, lambda { where("is_publish = ?", "0") }
  scope :publish, lambda { where("is_publish = ?" ,"1") }
  scope :all_showcases, lambda { where("is_publish = ? or is_publish = ?", "0", "1") }


  pg_search_scope :search,
                  :against => {
                    :title => 'A',
                    :info => 'B',
                    :mission_info => 'C'
                  },
                  :using => {
                    :tsearch => {:prefix => true}
                  }


  tracked owner: Proc.new{ |controller, model| controller.current_user }

  attr_accessible :title, :info, :only_invite, :reject_message,
                  :is_publish, :mission_term, :mission_info,
                  :contents_attributes, :showcase_rewards_attributes,
                  :reward_items


  #belongs_to :user
  #has_many :showcase_images, :dependent => :destroy
  #accepts_nested_attributes_for :showcase_images, :allow_destroy => true

  #has_many :image_archives, :as => :imageable

  belongs_to :user ,:foreign_key => 'created_by'
  has_many :showcase_rewards, :dependent => :destroy
  has_many :reward_items, :through => :showcase_rewards

  has_many :contents ,:dependent => :destroy
  has_many :favorite_items, :dependent => :destroy
  has_many :showcase_update_notifications, :dependent => :destroy
  has_many :agent_contracts, :dependent => :destroy
  has_many :notifications, :dependent => :destroy

  accepts_nested_attributes_for :contents, :showcase_rewards, :reward_items




  has_paper_trail

  acts_as_paranoid
  acts_as_commentable
  #acts_as_inkwell_post
  is_impressionable

  def favorite_count
    self.favorite_items.count
  end

  def self.text_search(query)
    if query.present?
      search(query)
    else
      scoped
    end
  end

end
