class ShowcaseImage < ActiveRecord::Base
  attr_accessible :image, :info, :title

  mount_uploader :image, ImageUploader

end
