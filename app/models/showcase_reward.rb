class ShowcaseReward < ActiveRecord::Base
  attr_accessible :deleted_at, :detail_info, :reward_item_id, :showcase_id

  #has_many :rewardable
  #has_many :reward_items, :through => :rewardable
  belongs_to :reward_item
  belongs_to :showcase

end
