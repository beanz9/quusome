class ShowcaseUpdateNotification < ActiveRecord::Base

  acts_as_paranoid

  attr_accessible :is_read, :showcase_id, :user_id

  belongs_to :showcase


end
