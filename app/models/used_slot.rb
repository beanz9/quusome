class UsedSlot < ActiveRecord::Base
  attr_accessible :agent_contract_id, :purchased_product_id

  belongs_to :agent_contract
end
