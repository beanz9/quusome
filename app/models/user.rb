class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable

  acts_as_paranoid
  acts_as_taggable
  acts_as_taggable_on :company_main_products
  #acts_as_inkwell_user
  acts_as_messageable

  #has_paper_trail

  #image_accessor :user_image


  after_destroy :notification_destroy


  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable#, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :country,               #국가
                  :name,                  #이름
                  :user_id,               #유저 아이디
                  :birth_year,            #생년
                  :birth_month,           #생월
                  :birth_day,             #생일
                  :gender,                #성별
                  :cell_phone_country,    #국제 코드
                  :cell_phone_1,          #모바일 전화 번호_1
                  :cell_phone_2,          #모바일 전화 번호_2
                  :cell_phone_3,          #모바일 전화 번호_3
                  :secondary_email,       #보조 이메일
                  :facebook_url,          #facebook url
                  :twitter_url,           #twitter url
                  :google_url,            #google url
                  :detail_info,           #상세 정보
                  :company_name,          #회사 명
                  :company_homepage,      #회사 홈페이지
                  :company_location,      #회사 소재지
                  :company_person,        #회사 담당자
                  :company_phone,         #회사 연락처
                  :company_main_product,  #회사 주력 상품 또는 브랜드
                  :company_detail_info,   #회사 상세 정보
                  :user_image_name,
                  :user_image_uid,
                  :user_image,
                  :image,
                  :selected_interest_areas_attributes,
                  :interest_areas_attributes,
                  :interest_area_ids




  mount_uploader :image, ImageUploader

  has_many :selected_interest_areas, :dependent => :destroy
  has_many :interest_areas, :through => :selected_interest_areas
  has_many :showcases, :foreign_key => 'created_by', :dependent => :destroy
  has_many :favorite_items, :dependent => :destroy
  has_many :conversation_users, :dependent => :destroy
  has_many :agent_contracts, :foreign_key => "agent_user_id", :dependent => :destroy
  has_many :agent_contracts_by_showcase_user, :class_name => 'AgentContract', :foreign_key => 'user_id',  :dependent => :destroy
  has_many :purchased_products, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :announcements, :dependent => :destroy

  #has_many :showcases, :dependent => :destroy

  #validates_presence_of :user_id, :email
  #validates_presence_of :password, :on => :create
  #validates_confirmation_of :password
  #validates_uniqueness_of :user_id, :email

  #Returning any kind of identification you want for the model
  #def name
  #  return "You should add method :name in your Messageable model"
  #end

  #Returning the email address of the model if an email should be sent for this object (Message or Notification).
  #If no mail has to be sent, return nil.
  def mailboxer_email(object)
    #Check if an email should be sent for that object
    #if true
    #return "define_email@on_your.model"
    #if false
    return nil
  end


  def inbox_unread_count(type)
    count = 0

    self.mailbox.inbox({:read => false}).each do |conversation|
      receipts =  conversation.receipts_for self
      receipts.each do |receipt|
        message = receipt.message
        if message.message_type == type
          count = count + 1
        end
      end
    end

    count
  end

  def inbox_count(type)
    count = 0
    self.mailbox.inbox({:read => false}).each do |conversation|
      receipts =  conversation.receipts_for self
      receipts.each do |receipt|
        message = receipt.message
        if message.message_type == type
          count = count + 1
        end
      end
    end
    count
  end

  def inbox_agent_request_count
    p inbox_count('agent_request')
  end

  def inbox_message_count
    p inbox_count('message')
  end

  def inbox_agent_request_unread_count
    inbox_unread_count('agent_request')
  end

  def inbox_message_unread_count
    inbox_unread_count('message')
  end

  def message_read_handler
    read_handler('message')
  end

  def agent_request_read_handler
    read_handler('agent_request')
  end

  def read_handler(type)
    self.mailbox.inbox({:read => false}).each do |conversation|
      receipts =  conversation.receipts_for self
      receipts.each do |receipt|
        message = receipt.message
        if message.message_type == type
          self.mark_as_read(message)
        end
      end
    end
  end

  def favorite(showcase)
    if self.favorite? showcase
      FavoriteItem.where(:user_id => self.id, :showcase_id => showcase.id).first.destroy
    else
      FavoriteItem.create(:user_id => self.id, :showcase_id => showcase.id)
    end

  end

  def unfavorite(showcase)
    return unless self.favorite? showcase
    FavoriteItem.where(:user_id => self.id, :showcase_id => showcase.id).destroy

  end

  def favorite?(showcase)
    FavoriteItem.where(:user_id => self.id, :showcase_id => showcase.id).first ? true : false
  end

  private

  def notification_destroy
    n = Notification.where("sender_id = ? or target_user_id = ?", self.id, self.id)
    n.destroy_all
  end
end

