class UserImage < ActiveRecord::Base

  image_accessor :user_image

  attr_accessible :user_id, :user_image_name, :user_image_uid


end
