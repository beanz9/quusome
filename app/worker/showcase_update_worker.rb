# encoding: UTF-8

class ShowcaseUpdateWorker
  include Sidekiq::Worker
  sidekiq_options retry: true


  def perform(showcase_id, current_user_id)
    favorite_items = FavoriteItem.where(:showcase_id => showcase_id)

    favorite_items.each do |item|

      showcase_update = ShowcaseUpdateNotification.new do |showcase|
        showcase.is_read = false,
        showcase.showcase_id = item.showcase_id,
        showcase.user_id = item.user_id
      end

      if showcase_update.save(item)
        showcase_update_count = ShowcaseUpdateNotification.where(:is_read => false, :user_id => item.user_id).count

        html = ''
        update = ShowcaseUpdateNotification.where(:user_id => item.user_id).order('created_at desc').page(1).per(5)
        update.each do |notification|
          if notification.is_read
            html.concat('<tr>')
          else
            html.concat('<tr style="background-color: #dddddd;">')
          end

          html.concat('<td style="width:40px;"><img src="' + notification.showcase.contents[0].url + '"></td>')
          html.concat('<td>')
          html.concat('<p>"' + notification.showcase.title + '"쇼케이스가 업데이트 되었습니다.</p>')
          html.concat('<p style="text-align: right;">' + notification.created_at.strftime('%Y-%m-%d') + '</p>')
          html.concat('<p>' + notification.id.to_s + '</p>')
          html.concat('</td>')
          html.concat('</tr>')
        end

        PrivatePub.publish_to "/showcase_update/" + item.user_id.to_s,
                              :showcase_update_count => showcase_update_count,
                              :html => html
      end

    end
  end
end