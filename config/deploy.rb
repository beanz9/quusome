require "bundler/capistrano"
require 'sidekiq/capistrano'
require 'new_relic/recipes'

load "config/recipes/base"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/postgresql"
load "config/recipes/nodejs"
load "config/recipes/rbenv"
load "config/recipes/imagemagick"
load "config/recipes/redis"
load "config/recipes/check"
load "config/recipes/private_pub"
load "config/recipes/seed"
load "config/recipes/timezone"


#server "210.122.0.59", :web, :app, :db, :redis, primary: true
server "198.199.104.207", :web, :app, :db, :redis, primary: true

set :user, "quusome"
set :application, "quusome"
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false
set :ruby_version, "1.9.3-p392"
set :rails_env, "production"




set :scm, "git"
set :repository, "https://beanz9@bitbucket.org/beanz9/quusome.git"
set :branch, "master"

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
#ssh_options[:port] = 2222


after "deploy", "deploy:cleanup" # keep only the last 5 releases

def press_enter( ch, stream, data)
  if data =~ /Press.\[ENTER\].to.continue/
    # prompt, and then send the response to the remote process
    ch.send_data( "\n")
  else
    # use the default handler for all other text
    Capistrano::Configuration.default_io_proc.call( ch, stream, data)
  end
end

after "deploy:update", "newrelic:notice_deployment"

