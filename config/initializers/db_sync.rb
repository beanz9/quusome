DbSync.configure do |config|
  config.sync_tables = ["abouts", "active_admin_comments", "activities", "admin_users",
  "agent_contracts", "agreements", "comments", "contents", "conversation_users", "conversations",
  "favorite_items", "impressions", "interest_areas", "notifications", "pg_search_documents",
  "product_items", "purchased_products", "receipts", "reward_items", "schema_migrations",
  "selected_interest_areas", "showcase_images", "showcase_rewards", "showcase_update_notifications",
  "showcases", "taggings", "tags", "used_slots", "users", "versions"]
end