module Inkwell
  class Engine < Rails::Engine
    config.post_table = :showcases
    config.user_table = :users
    #config.community_table = :communities #if you want to use communities
  end
end