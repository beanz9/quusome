namespace :private_pub do
  desc "Start private_pub server"
  task :start do
    #run "cd #{current_path};RAILS_ENV=production bundle exec rackup private_pub.ru -s thin -E production -D -P tmp/pids/private_pub.pid"
    run "cd #{current_path};RAILS_ENV=production bundle exec thin -C config/private_pub_thin.yml -d start"
  end
 
  desc "Stop private_pub server"
  task :stop do
    #run "cd #{current_path};if [ -f tmp/pids/private_pub.pid ] && [ -e /proc/$(cat tmp/pids/private_pub.pid) ]; then kill -9 `cat tmp/pids/private_pub.pid`; fi"
    run "cd #{current_path};RAILS_ENV=production bundle exec thin -C config/private_pub_thin.yml stop"
  end
 
  desc "Restart private_pub server"
  task :restart do
    stop
    start
  end

  #after "unicorn:start", "private_pub:start"
  #after "unicorn:restart", "private_pub:restart"
  #after "unicorn:stop", "private_pub:stop"

end