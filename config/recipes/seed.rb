namespace :deploy do
  desc "reload the database with seed data"
  task :seed, :roles => :db do
    run "cd #{current_path}; bundle exec rake db:seed_fu RAILS_ENV=#{rails_env}"
  end

  after "deploy:migrate", "deploy:seed"
end