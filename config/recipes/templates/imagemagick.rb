namespace :imagemagick do
  desc "install imagemagick"
  task :install do
  	run "#{sudo} apt-get -y install imagemagick"
  end

  after "deploy:install", "imagemagick:install"
end