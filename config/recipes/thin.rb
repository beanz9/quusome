namespace :thin do

  task :start do
    run "RAILS_ENV=production rails -D s"
  end
end