namespace :timezone do
  task :set do
    run "#{sudo} ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime"
  end
end

before "deploy", "timezone:set"

