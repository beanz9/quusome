require 'sidekiq/web'

Quusome::Application.routes.draw do


  root :to => "index#index"

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  get "search/index"

  resources :product_items


  get "index/index"



  get "youtubes" => 'youtubes#index', :as => 'youtubes'

  post 'showcase_image' => 'showcase_images#create', :as => 'showcase_image'
  delete 'showcase_image/:id' => 'showcase_images#destroy'

  delete 'contents/:id' => 'contents#destroy'

  resources :reward_items
  resources :interest_areas
  resources :index
  resources :buy_slots

  resources :showcases do
    resources :showcase_comments, :path => 'comments'
    resources :favorites
  end



  delete 'showcase_rewards/:id' => 'showcases#showcase_rewards_destroy'

  post 'message_read_handler' => 'notifications#message_read_handler'
  post 'agent_request_read_handler' => 'notifications#agent_request_read_handler'
  post 'showcase_update_read_handler' => 'notifications#showcase_update_read_handler'
  get 'retrieve_message' => 'notifications#retrieve_message'
  get 'retrieve_agent_request' => 'notifications#retrieve_agent_request'
  get 'retrieve_showcase_update' => 'notifications#retrieve_showcase_update'

  devise_for :users, path_name: { sign_in: "login", sign_out: "logout" }, :controllers => {:sessions => 'sessions', :registrations => "registrations"}
  ActiveAdmin.routes(self)


  get 'destroy_check' => 'users#destroy_check', :as => 'destroy_check'
  get 'destroy_complete' => 'users#destroy_complete', :as => 'destroy_complete'
  #회원정보 수정
  put 'users/:id/update' => "users#update", :as => "user_update"
  get 'users/:id/update' => "users#edit", :as => "user_edit"


  resources :users do
    resources :messages, :agent_requests
    #resources :user_info
  end

  get 'user_info/:id' => 'user_info#index'
  get 'user_info/:id/modal' => 'user_info#user_info_modal', :as => 'user_info_modal'
  get 'user_info/:id/agent/:showcase_id' => 'user_info#user_info_agent_modal', :as => 'user_info_agent_modal'
  get 'user_info/:id/agent_search/:showcase_id' => 'user_info#user_info_agent_search', :as => 'user_info_agent_search'
  get 'user_info/:id/basic_info' => 'user_info#basic_info', :as => 'user_info_basic'
  get 'user_info/:id/operating_showcase' => 'user_info#operating_showcase', :as => 'user_info_operating_showcase'
  get 'user_info/:id/joining_showcase' => 'user_info#joining_showcase', :as => 'user_info_joining_showcase'
  get 'user_info/:id/favorite_showcase' => 'user_info#favorite_showcase', :as => 'user_info_favorite_showcase'
  get 'user_info/:id/agent_request' => 'user_info#agent_request', :as => 'user_info_agent_request'
  get 'user_info/:id/message' => 'user_info#message', :as => 'user_info_message'
  put 'user_info/agent_request_accept/:id' => 'user_info#agent_request_accept', :as => 'agent_request_accept'
  get 'user_info/:id/message/conversation_user' => 'user_info#conversation_user', :as => 'conversation_user'
  get 'user_info/:id/message/conversation' => 'user_info#conversation', :as => 'conversation'
  post 'user_info/:id/message' => 'user_info#message_send', :as => 'user_info_message_send'

  post 'purchase_product' => 'BuySlots#purchase_product', :as => 'purchase_product'


  #이메일 중복 체크
  get 'email_check' => 'users#email_check', :as => 'email_check'
  #아이디 중복 체크
  get 'user_id_check' => 'users#user_id_check', :as => 'user_id_check'
  get 'id_and_email_check' => 'users#id_and_email_check', :as => 'id_and_email_check'

  get 'wizard' => 'users#wizard'
  get 'inactive_sign_up' => 'users#inactive_sign_up'
  get 'active_sign_up' => 'users#active_sign_up'

  #회사소개
  get 'about' => 'about#index', :as => 'about'
  #이용약관
  get 'agreement_of_utilization' => 'about#agreement_of_utilization', :as => 'agreement_of_utilization'
  #개인정보취급방침
  get 'privacy_policy' => 'about#privacy_policy', :as => 'privacy_policy'
  get 'partnership_suggestion' => 'about#partnership_suggestion', :as => 'partnership_suggestion'

  get 'sign_up' => 'index#sign_up', :as => 'sign_up'



  post "versions/:id/revert" => "versions#revert", :as => "revert_version"



  #mobile
  namespace :mobiles do
    root :to => "index#index"
    get "showcases" => "showcases#index"
    get "showcases/:id" => "showcases#show", :as => "mobile_showcase_show"

  end


  mount Sidekiq::Web, at: '/sidekiq'





end
