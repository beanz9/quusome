InterestArea.delete_all

InterestArea.seed(:id,
  {:id => 1, :title => '패션', :desc => '패션'},
  {:id => 2, :title => '영화/연극/공연', :desc => '영화/연극/공연'},
  {:id => 3, :title => '스포츠', :desc => '스포츠'},
  {:id => 4, :title => '방송/연예인', :desc => '방송/연예인'},
  {:id => 5, :title => '스마트기기/IT', :desc => '스마트기기/IT'},
  {:id => 6, :title => '인물/문화', :desc => '인물/문화'},
  {:id => 7, :title => '자동차/기계', :desc => '자동차/기계'},
  {:id => 8, :title => '교육/강의', :desc => '교육/강의'},
  {:id => 9, :title => '음식/맛집/요리', :desc => '음식/맛집/요리'},
  {:id => 10, :title => '디자인/팬시', :desc => '디자인/팬시'},
  {:id => 11, :title => '가구/인테리어', :desc => '가구/인테리어'},
  {:id => 12, :title => '캠페인/행사', :desc => '캠페인/행사'}
)