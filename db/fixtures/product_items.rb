ProductItem.delete_all

ProductItem.seed(:id,
  {:id => 1, :product_name => '10개/1M', :product_info => '결제일로부터 30일간 10개 슬롯을 사용할 수 있는 상품입니다.', :product_price => '5000', :product_color => 'red', :slot_count => 10},
  {:id => 2, :product_name => '20개/1M', :product_info => '결제일로부터 30일간 20개 슬롯을 사용할 수 있는 상품입니다.', :product_price => '10000', :product_color => 'orange', :slot_count => 20},
  {:id => 3, :product_name => '30개/1M', :product_info => '결제일로부터 30일간 30개 슬롯을 사용할 수 있는 상품입니다.', :product_price => '15000', :product_color => 'green', :slot_count => 30},
  {:id => 4, :product_name => '무제한/1M', :product_info => '결제일로부터 30일간 무제한으로 슬롯을 사용할 수 있는 상품입니다.', :product_price => '29800', :product_color => 'blue', :slot_count => 0}
)