RewardItem.delete_all

RewardItem.seed(:id,
  {:id => 1, :title => '감사인사', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'thank.jpg'},
  {:id => 2, :title => '음원쿠폰', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'music.jpg'},
  {:id => 3, :title => '커피쿠폰', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'coffee.jpg'},
  {:id => 4, :title => '티켓', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'ticket.jpg'},
  {:id => 5, :title => '무료샘플', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'freesample.jpg'},
  {:id => 6, :title => '테스트샘플', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'costsample.jpg'},
  {:id => 7, :title => '할인가격', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'saleprice.jpg'},
  {:id => 8, :title => '특별공급가격', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'specialprice.jpg'},
  {:id => 9, :title => '판매권', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'right.jpg'},
  {:id => 10, :title => '독점판매권', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'indiright.jpg'},
  {:id => 11, :title => '활동비/수수료', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'money.jpg'},
  {:id => 12, :title => '기타', :description => '선물에 따른 수량, 선/후불, 금액 등 세부사항을 기재해주세요.', :image => 'etc.jpg'}

)