class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :user_id
      t.string :name
      t.string :country
      t.string :birth_year
      t.string :birth_month
      t.string :birth_day
      t.string :gender
      t.string :cell_phone_country
      t.string :cell_phone_1
      t.string :cell_phone_2
      t.string :cell_phone_3
      t.string :secondary_email
      t.string :facebook_url
      t.string :twitter_url
      t.string :google_url
      t.text :detail_info
      t.string :company_name
      t.string :company_homepage
      t.string :company_location
      t.string :company_person
      t.string :company_phone
      t.text :company_main_product
      t.text :company_detail_info

      t.timestamps
    end
  end
end
