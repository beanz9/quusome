class CreateUserImages < ActiveRecord::Migration
  def change
    create_table :user_images do |t|
      t.integer :user_id
      t.string :user_image_name
      t.string :user_image_uid

      t.timestamps
    end
  end
end
