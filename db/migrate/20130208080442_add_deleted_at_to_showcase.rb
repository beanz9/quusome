class AddDeletedAtToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :deleted_at, :timestamp
  end
end
