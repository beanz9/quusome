class AddUserImageNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :user_image_name, :string
    add_column :users, :user_image_uid, :string
  end
end
