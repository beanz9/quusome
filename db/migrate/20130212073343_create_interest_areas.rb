class CreateInterestAreas < ActiveRecord::Migration
  def change
    create_table :interest_areas do |t|
      t.string :title
      t.text :desc

      t.timestamps
    end
  end
end
