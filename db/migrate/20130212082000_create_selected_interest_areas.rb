class CreateSelectedInterestAreas < ActiveRecord::Migration
  def change
    create_table :selected_interest_areas do |t|
      t.integer :user_id
      t.integer :interest_area_id

      t.timestamps
    end
  end
end
