class AddDeletedAtToSelectedInterestArea < ActiveRecord::Migration
  def change
    add_column :selected_interest_areas, :deleted_at, :timestamp
  end
end
