class CreateRewardItems < ActiveRecord::Migration
  def change
    create_table :reward_items do |t|
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
