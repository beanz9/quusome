class AddImageToRewardItem < ActiveRecord::Migration
  def change
    add_column :reward_items, :image, :string
  end
end
