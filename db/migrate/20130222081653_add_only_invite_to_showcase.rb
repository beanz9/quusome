class AddOnlyInviteToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :only_invite, :string
  end
end
