class AddRejectMessageToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :reject_message, :string
  end
end
