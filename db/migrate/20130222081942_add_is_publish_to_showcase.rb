class AddIsPublishToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :is_publish, :string
  end
end
