class AddMissionTermToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :mission_term, :string
  end
end
