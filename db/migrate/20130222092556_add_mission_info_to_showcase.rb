class AddMissionInfoToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :mission_info, :string
  end
end
