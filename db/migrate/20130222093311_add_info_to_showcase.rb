class AddInfoToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :info, :text
  end
end
