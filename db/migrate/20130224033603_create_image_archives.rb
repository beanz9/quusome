class CreateImageArchives < ActiveRecord::Migration
  def change
    create_table :image_archives do |t|
      t.text :info
      t.string :image
      t.integer :imageable_id
      t.string :imageable_type

      t.timestamps
    end
  end
end
