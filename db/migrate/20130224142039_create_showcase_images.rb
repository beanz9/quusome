class CreateShowcaseImages < ActiveRecord::Migration
  def change
    create_table :showcase_images do |t|
      t.string :title
      t.string :image
      t.text :info

      t.timestamps
    end
  end
end
