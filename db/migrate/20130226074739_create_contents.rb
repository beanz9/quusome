class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.string :source_type
      t.integer :showcase_image_id
      t.text :url
      t.text :caption
      t.integer :showcase_id
      t.integer :order
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
