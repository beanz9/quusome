class AddSourceIdToContent < ActiveRecord::Migration
  def change
    add_column :contents, :source_id, :string
  end
end
