class CreateRewardables < ActiveRecord::Migration
  def change
    create_table :rewardables do |t|
      t.integer :agent_id
      t.integer :reward_item_id

      t.timestamps
    end
  end
end
