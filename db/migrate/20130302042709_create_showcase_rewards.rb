class CreateShowcaseRewards < ActiveRecord::Migration
  def change
    create_table :showcase_rewards do |t|
      t.integer :showcase_id
      t.text :detail_info
      t.integer :reward_item_id
      t.date :deleted_at

      t.timestamps
    end
  end
end
