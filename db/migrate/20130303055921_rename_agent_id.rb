class RenameAgentId < ActiveRecord::Migration
  def up
    rename_column :rewardables, :agent_id, :showcase_reward_id
  end

  def down
  end
end
