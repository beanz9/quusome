class AddCreatedByToShowcase < ActiveRecord::Migration
  def change
    add_column :showcases, :created_by, :integer
    add_column :showcases, :updated_by, :integer
  end
end
