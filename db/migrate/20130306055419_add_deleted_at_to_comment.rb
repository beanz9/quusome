class AddDeletedAtToComment < ActiveRecord::Migration
  def change
    add_column :comments, :deleted_at, :date
  end
end
