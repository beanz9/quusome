class CreateFavoriteItems < ActiveRecord::Migration
  def change
    create_table :favorite_items do |t|
      t.integer :showcase_id
      t.integer :user_id

      t.timestamps
    end
  end
end
