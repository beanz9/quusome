class AddIsAcceptToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :is_accept, :boolean
  end
end
