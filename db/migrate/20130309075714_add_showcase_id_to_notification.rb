class AddShowcaseIdToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :showcase_id, :integer
  end
end
