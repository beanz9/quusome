class CreateShowcaseUpdateNotifications < ActiveRecord::Migration
  def change
    create_table :showcase_update_notifications do |t|
      t.boolean :is_read
      t.integer :showcase_id

      t.timestamps
    end
  end
end
