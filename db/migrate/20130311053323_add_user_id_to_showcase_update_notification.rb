class AddUserIdToShowcaseUpdateNotification < ActiveRecord::Migration
  def change
    add_column :showcase_update_notifications, :user_id, :integer
  end
end
