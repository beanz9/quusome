class ChangeTitleTypeInShowcase < ActiveRecord::Migration
  def change
    change_column :showcases, :title, :text, :limit => nil
  end

end
