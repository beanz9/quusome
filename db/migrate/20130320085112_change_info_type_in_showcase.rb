class ChangeInfoTypeInShowcase < ActiveRecord::Migration
  def change
    change_column :showcases, :info, :text, :limit => nil
  end
end
