class ChangeMissionInfoTypeInShowcases < ActiveRecord::Migration
  def change
    change_column :showcases, :mission_info, :text, :limit => nil
  end
end
