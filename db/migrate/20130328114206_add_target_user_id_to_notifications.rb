class AddTargetUserIdToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :target_user_id, :integer
  end
end
