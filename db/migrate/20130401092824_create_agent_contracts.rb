class CreateAgentContracts < ActiveRecord::Migration
  def change
    create_table :agent_contracts do |t|
      t.integer :agent_user_id
      t.integer :showcase_id
      t.timestamp :start_date
      t.timestamp :end_date

      t.timestamps
    end
  end
end
