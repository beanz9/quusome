class CreateProductItems < ActiveRecord::Migration
  def change
    create_table :product_items do |t|
      t.string :product_name
      t.text :product_info
      t.string :product_price

      t.timestamps
    end
  end
end
