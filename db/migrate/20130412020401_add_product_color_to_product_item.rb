class AddProductColorToProductItem < ActiveRecord::Migration
  def change
    add_column :product_items, :product_color, :string
  end
end
