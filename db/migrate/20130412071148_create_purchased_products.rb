class CreatePurchasedProducts < ActiveRecord::Migration
  def change
    create_table :purchased_products do |t|
      t.integer :user_id
      t.timestamp :start_date
      t.timestamp :end_date
      t.integer :product_item_id

      t.timestamps
    end
  end
end
