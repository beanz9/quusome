class AddSlotCountToProductItem < ActiveRecord::Migration
  def change
    add_column :product_items, :slot_count, :integer
  end
end
