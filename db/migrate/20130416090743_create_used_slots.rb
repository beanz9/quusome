class CreateUsedSlots < ActiveRecord::Migration
  def change
    create_table :used_slots do |t|
      t.integer :agent_contract_id
      t.integer :purchased_item_id

      t.timestamps
    end
  end
end
