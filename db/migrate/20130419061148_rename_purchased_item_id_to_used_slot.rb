class RenamePurchasedItemIdToUsedSlot < ActiveRecord::Migration
  def up
    rename_column :used_slots, :purchased_item_id, :purchased_product_id
  end

  def down
  end
end
