class AddUserIdToAgentContract < ActiveRecord::Migration
  def change
    add_column :agent_contracts, :user_id, :integer
  end
end
