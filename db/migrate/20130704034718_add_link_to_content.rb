class AddLinkToContent < ActiveRecord::Migration
  def change
    add_column :contents, :link, :text
  end
end
