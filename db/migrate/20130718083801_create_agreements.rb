class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.text :description

      t.timestamps
    end
  end
end
