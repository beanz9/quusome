class AddTitleToAgreement < ActiveRecord::Migration
  def change
    add_column :agreements, :title, :string
  end
end
