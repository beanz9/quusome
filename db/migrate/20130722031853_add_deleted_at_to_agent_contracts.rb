class AddDeletedAtToAgentContracts < ActiveRecord::Migration
  def change
    add_column :agent_contracts, :deleted_at, :time
  end
end
