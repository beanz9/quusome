class AddDeletedAtToFavoriteItems < ActiveRecord::Migration
  def change
    add_column :favorite_items, :deleted_at, :time
  end
end
