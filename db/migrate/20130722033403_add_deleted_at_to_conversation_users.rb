class AddDeletedAtToConversationUsers < ActiveRecord::Migration
  def change
    add_column :conversation_users, :deleted_at, :time
  end
end
