class AddDeletedAtToPurchasedProducts < ActiveRecord::Migration
  def change
    add_column :purchased_products, :deleted_at, :time
  end
end
