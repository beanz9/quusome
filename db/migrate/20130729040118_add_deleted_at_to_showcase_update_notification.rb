class AddDeletedAtToShowcaseUpdateNotification < ActiveRecord::Migration
  def change
    add_column :showcase_update_notifications, :deleted_at, :time
  end
end
