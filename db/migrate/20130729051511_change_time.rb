class ChangeTime < ActiveRecord::Migration
  def change

    remove_column :showcase_update_notifications, :deleted_at
    remove_column :agent_contracts, :deleted_at
    remove_column :favorite_items, :deleted_at
    remove_column :conversation_users, :deleted_at
    remove_column :purchased_products, :deleted_at
    remove_column :notifications, :deleted_at

  end
end
