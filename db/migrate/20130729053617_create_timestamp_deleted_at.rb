class CreateTimestampDeletedAt < ActiveRecord::Migration
  def change

    add_column :showcase_update_notifications, :deleted_at, :timestamp
    add_column :agent_contracts, :deleted_at, :timestamp
    add_column :favorite_items, :deleted_at, :timestamp
    add_column :conversation_users, :deleted_at, :timestamp
    add_column :purchased_products, :deleted_at, :timestamp
    add_column :notifications, :deleted_at, :timestamp


  end
end
