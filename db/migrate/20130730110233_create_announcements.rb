class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.text :body
      t.string :title
      t.boolean :is_public
      t.string :type
      t.timestamp :expire_date
      t.integer :user_id
      t.boolean :is_read

      t.timestamps
    end
  end
end
