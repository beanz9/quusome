class RemoveTypeFromAnnouncement < ActiveRecord::Migration
  def up
    remove_column :announcements, :type
  end

  def down
  end
end
