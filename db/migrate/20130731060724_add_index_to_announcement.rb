class AddIndexToAnnouncement < ActiveRecord::Migration
  def change
    add_index :announcements, :is_public,                :unique => false
    add_index :announcements, :is_read,                :unique => false
    add_index :announcements, :expire_date,                :unique => false
    add_index :announcements, :user_id,                :unique => false
  end
end
