class AddDeletedAtToAnnouncement < ActiveRecord::Migration
  def change
    add_column :announcements, :deleted_at, :timestamp

    add_index :announcements, :deleted_at, :unique => false
  end
end
