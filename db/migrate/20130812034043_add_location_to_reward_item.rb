class AddLocationToRewardItem < ActiveRecord::Migration
  def change
    add_column :reward_items, :location, :string
  end
end
