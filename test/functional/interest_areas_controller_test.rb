require 'test_helper'

class InterestAreasControllerTest < ActionController::TestCase
  setup do
    @interest_area = interest_areas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:interest_areas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create interest_area" do
    assert_difference('InterestArea.count') do
      post :create, interest_area: { desc: @interest_area.desc, title: @interest_area.title }
    end

    assert_redirected_to interest_area_path(assigns(:interest_area))
  end

  test "should show interest_area" do
    get :show, id: @interest_area
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @interest_area
    assert_response :success
  end

  test "should update interest_area" do
    put :update, id: @interest_area, interest_area: { desc: @interest_area.desc, title: @interest_area.title }
    assert_redirected_to interest_area_path(assigns(:interest_area))
  end

  test "should destroy interest_area" do
    assert_difference('InterestArea.count', -1) do
      delete :destroy, id: @interest_area
    end

    assert_redirected_to interest_areas_path
  end
end
