require 'test_helper'

class RewardItemsControllerTest < ActionController::TestCase
  setup do
    @reward_item = reward_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reward_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reward_item" do
    assert_difference('RewardItem.count') do
      post :create, reward_item: { description: @reward_item.description, title: @reward_item.title }
    end

    assert_redirected_to reward_item_path(assigns(:reward_item))
  end

  test "should show reward_item" do
    get :show, id: @reward_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reward_item
    assert_response :success
  end

  test "should update reward_item" do
    put :update, id: @reward_item, reward_item: { description: @reward_item.description, title: @reward_item.title }
    assert_redirected_to reward_item_path(assigns(:reward_item))
  end

  test "should destroy reward_item" do
    assert_difference('RewardItem.count', -1) do
      delete :destroy, id: @reward_item
    end

    assert_redirected_to reward_items_path
  end
end
