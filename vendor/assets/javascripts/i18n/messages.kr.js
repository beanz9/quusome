window.ParsleyConfig = window.ParsleyConfig || {};

(function ($) {
  window.ParsleyConfig = $.extend( true, {}, window.ParsleyConfig, {
    messages: {
      // parsley //////////////////////////////////////
        defaultMessage: "This value seems to be invalid."
        , type: {
            email:      "* 이메일 형식에 맞게 입력해 주세요."
            , url:        "This value should be a valid url."
            , urlstrict:  "This value should be a valid url."
            , number:     "This value should be a valid number."
            , digits:     "This value should be digits."
            , dateIso:    "This value should be a valid date (YYYY-MM-DD)."
            , alphanum:   "This value should be alphanumeric."
        }
        , notnull:        "This value should not be null."
        , notblank:       "This value should not be blank."
        , required:       "* 필수로 입력하는 항목입니다."
        , regexp:         "This value seems to be invalid."
        , min:            "This value should be greater than %s."
        , max:            "This value should be lower than %s."
        , range:          "This value should be between %s and %s."
        , minlength:      "This value is too short. It should have %s characters or more."
        , maxlength:      "This value is too long. It should have %s characters or less."
        , rangelength:    "This value length is invalid. It should be between %s and %s characters long."
        , mincheck:       "You must select at least %s choices."
        , maxcheck:       "You must select %s choices or less."
        , rangecheck:     "You must select between %s and %s choices."
        , equalto:        "* 입력한 항목이 일치해야 합니다.."

      // parsley.extend ///////////////////////////////
      , minwords: "Este valor debe tener al menos %s palabras."
      , maxwords: "Este valor no debe exceder las %s palabras."
      , rangewords: "Este valor debe tener entre %s y %s palabras."
      , greaterthan: "Este valor no debe ser mayor que %s."
      , lessthan: "Este valor no debe ser menor que %s."
    }
  });
}(window.jQuery || window.Zepto));